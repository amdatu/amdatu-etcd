== Resources

[cols="40%,60%"]
|===
|Tools|Location
|Source Code|https://bitbucket.org/amdatu/amdatu-etcd[^]
|Issue Tracking|https://amdatu.atlassian.net/browse/AMDETCD[^]
|Continuous Build|https://bitbucket.org/amdatu/amdatu-etcd/addon/pipelines/home[^]
|===
