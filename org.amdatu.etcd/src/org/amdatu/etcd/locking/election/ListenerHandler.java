/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election;

import static org.amdatu.etcd.util.EtcdUtil.isEmpty;
import static org.osgi.service.log.LogService.LOG_DEBUG;

import org.amdatu.etcd.locking.ElectionEvent;
import org.amdatu.etcd.locking.ElectionListener;

/**
 * An internal delegate that handles a single Election Listener registration.
 * 
 * @see ElectionListener
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ListenerHandler extends DelegateBase implements ElectionListener {

    private final ElectionListener m_listener;
    private final String m_topic;

    public ListenerHandler(ElectionComponent component, ElectionListener listener, String topic) {
        super(component, ListenerHandler.class.getSimpleName());
        m_listener = listener;
        m_topic = topic;
    }

    @Override
    public void startDelegate() {
        log(LOG_DEBUG, "%s: Starting handler - topic=%s", getName(), m_topic);
    }

    @Override
    public void stopDelegate() {
        log(LOG_DEBUG, "%s: Stopping handler - topic=%s", getName(), m_topic);
    }

    public ElectionListener getListener() {
        return m_listener;
    }

    public String getTopic() {
        return m_topic;
    }

    @Override
    public void handle(ElectionEvent event) {
        if (isActive() && (isEmpty(m_topic) || event.getTopic().equals(m_topic))) {
            m_listener.handle(event);
        }
    }
}
