/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.LEADER_CHANGED;
import static org.amdatu.etcd.util.EtcdUtil.cancel;
import static org.amdatu.etcd.util.EtcdUtil.getModified;
import static org.amdatu.etcd.util.EtcdUtil.isEmpty;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.util.concurrent.Future;

import org.amdatu.etcd.locking.ElectionCandidate;
import org.amdatu.etcd.locking.ElectionEvent;
import org.amdatu.etcd.locking.ElectionListener;

import mousio.etcd4j.responses.EtcdException;
import mousio.etcd4j.responses.EtcdKeysResponse;

/**
 * An internal delegate that handles a single Election Candidate registration.
 * <p/>
 * A Candidate Handler is responsible for registering (and unregistering) the
 * Election Candidate in ETCD as well as invoking callbacks on it.
 *
 * @see ElectionListener
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class CandidateHandler extends DelegateBase implements ElectionListener {

    private final ElectionCandidate m_candidate;
    private final String m_identity;
    private final String m_topic;
    private final String m_topicPath;

    private volatile Future<?> m_initFuture;
    private volatile Future<?> m_updateElectionPathFuture;
    private volatile Future<?> m_updateCandidatePathFuture;

    private volatile boolean m_leader = false;
    private volatile String m_candidatePath;

    public CandidateHandler(ElectionComponent component, ElectionCandidate candidate, String topic, String identity) {
        super(component, CandidateHandler.class.getSimpleName());
        m_candidate = candidate;
        m_identity = identity;
        m_topic = topic;
        m_topicPath = getTopicPath(m_topic);
    }

    @Override
    public void startDelegate() {
        log(LOG_DEBUG, "%s: Starting candidate - topic=%s identity=%s", getName(), m_topic, m_identity);
        getExecutor().execute(new InitTask());
    }

    @Override
    public void stopDelegate() {
        log(LOG_DEBUG, "%s: Stopping candidate - topic=%s identity=%s", getName(), m_topic, m_identity);
        cancel(m_initFuture, m_updateElectionPathFuture, m_updateCandidatePathFuture);
        if (m_leader) {
            m_leader = false;
            m_candidate.demoted();
        }
        clearMembership();
    }

    public String getTopic() {
        return m_topic;
    }

    public String getIdentity() {
        return m_identity;
    }

    @Override
    public void handle(ElectionEvent event) {
        if (isActive() && event.getType() == LEADER_CHANGED && event.getTopic().equals(m_topic)) {
            if (m_identity.equals(event.getIdentity())) {
                m_leader = true;
                // immediately take-over leader responsibilities
                cancel(m_initFuture, m_updateElectionPathFuture, m_updateCandidatePathFuture);
                getExecutor().execute(new UpdateCandidatePathTask());
                getExecutor().execute(new UpdateElectionPathTask());
                m_candidate.elected();
            }
            else if (m_leader) {
                m_leader = false;
                m_candidate.demoted();
            }
        }
    }

    private void clearMembership() {
        String candidatePath = m_candidatePath;
        if (isEmpty(candidatePath)) {
            return;
        }
        try {
            EtcdKeysResponse response =
                getEtcdClient().delete(candidatePath).timeout(getRequestTimeout(), SECONDS).send().get();
            m_candidatePath = null;
            log(LOG_DEBUG, "%s: Removed candidate path - topic=%s candidate=%s path=%s mod=%s", getName(), m_topic,
                m_identity, m_topicPath + "/" + candidatePath, getModified(response));
        }
        catch (EtcdException e) {
            if (e.errorCode != 100) {
                log(LOG_WARNING, "%s: Failed to remove candidate path - topic=%s candidate=%s! Resetting...", e,
                    getName(), m_topic, m_identity);
            }
        }
        catch (Exception e) {
            log(LOG_WARNING, "%s: Failed to remove candidate path - topic=%s candidate=%s! Resetting...", e, getName(),
                m_topic, m_identity);
        }
    }

    private class InitTask implements Runnable {

        @Override
        public void run() {

            if (!isActive()) {
                log(LOG_WARNING, "%s: Handler inactive.. cancelling init task", getName());
                return;
            }

            try {
                EtcdKeysResponse response = getEtcdClient().putDir(m_topicPath).prevExist(false).ttl(getTopicTTL())
                    .timeout(getRequestTimeout(), SECONDS).send().get();
                log(LOG_DEBUG, "%s: Created election path - topic=%s identity=%s key=%s at index %s", getName(),
                    m_topic, m_identity, response.node.key, getModified(response));
            }
            catch (EtcdException e) {
                if (e.errorCode != 105) {
                    log(LOG_WARNING, "%s: Failed to create election path - topic=%s identity=%s path=%s. Retrying...",
                        e, getName(), m_topic, m_identity, m_topicPath);
                    m_initFuture = getExecutor().schedule(this, getRetryDelay(), SECONDS);
                    return;
                }
                log(LOG_DEBUG, "%s: Election path exists - topic=%s identity=%s key=%s", getName(), m_topic,
                    m_identity, m_topicPath);
            }
            catch (Exception e) {
                log(LOG_WARNING, "%s: Failed to create election path - topic=%s identity=%s path=%s. Retrying...", e,
                    getName(), m_topic, m_identity, m_topicPath);
                m_initFuture = getExecutor().schedule(this, getRetryDelay(), SECONDS);
                return;
            }

            try {
                EtcdKeysResponse response = getEtcdClient().post(m_topicPath, m_identity).ttl(getCandidateTTL())
                    .timeout(getRequestTimeout(), SECONDS).send().get();
                m_candidatePath = response.node.key;
                m_updateElectionPathFuture = getExecutor().schedule(new UpdateElectionPathTask(),
                    getCandidateTTL() - getUpdateMargin(), SECONDS);
                m_updateCandidatePathFuture = getExecutor().schedule(new UpdateCandidatePathTask(),
                    getCandidateTTL() - getUpdateMargin(), SECONDS);
                log(LOG_DEBUG, "%s: Created candidate path - topic=%s identity=%s key=%s mod=%s ttl=%s", getName(),
                    m_topic, m_identity, response.node.key, getModified(response), getCandidateTTL());
            }
            catch (Exception e) {
                log(LOG_WARNING, "%s: Failed to init candidate path topic=%s identity=%s! Resetting...", e, getName(),
                    m_topic, m_identity);
                m_initFuture = getExecutor().schedule(this, getRetryDelay(), SECONDS);
            }
        }
    }

    private class UpdateElectionPathTask implements Runnable {

        @Override
        public void run() {

            if (!isActive()) {
                log(LOG_WARNING, "%s: Handler inactive.. cancelling init task", getName());
                return;
            }

            try {
                if (m_leader) {
                    EtcdKeysResponse response = getEtcdClient().putDir(m_topicPath).prevExist(true).ttl(getTopicTTL())
                        .timeout(getRequestTimeout(), SECONDS).send().get();
                    m_updateElectionPathFuture =
                        getExecutor().schedule(this, getCandidateTTL() - getUpdateMargin(), SECONDS);
                    log(LOG_DEBUG, "%s: Updated election path - topic=%s candidate=%s key=%s mod=%s ttl=%s", getName(),
                        m_topic, m_identity, response.node.key, getModified(response), getTopicTTL());
                }
            }
            catch (Exception e) {
                log(LOG_WARNING, "%s: Failed to update election path - topic=%s candidate=%s! Resetting...", e,
                    getName(), m_topic, m_identity);
                cancel(m_initFuture, m_updateElectionPathFuture, m_updateCandidatePathFuture);
                clearMembership();
                m_candidatePath = null;
                m_initFuture = getExecutor().schedule(new InitTask(), getRetryDelay(), SECONDS);
            }
        }
    }

    private class UpdateCandidatePathTask implements Runnable {

        @Override
        public void run() {
            if (!isActive()) {
                log(LOG_WARNING, "%s: Handler inactive.. cancelling candidatepath update task", getName());
                return;
            }
            String candidatePath = m_candidatePath;
            if (isEmpty(candidatePath)) {
                log(LOG_DEBUG, "%s: Candidate key empty.. cancelling update candidatepath update task", getName());
                return;
            }
            boolean leader = m_leader;
            try {
                if (leader) {
                    EtcdKeysResponse response = getEtcdClient().put(candidatePath, m_identity).prevExist(true)
                        .ttl(getLeaderTTL()).timeout(getRequestTimeout(), SECONDS).send().get();
                    m_updateCandidatePathFuture =
                        getExecutor().schedule(this, getLeaderTTL() - getUpdateMargin(), SECONDS);
                    log(LOG_DEBUG, "%s: Updated leader path - topic=%s candidate=%s key=%s mod=%s ttl=%s", getName(),
                        m_topic, m_identity, response.node.key, getModified(response), getLeaderTTL());
                }
                else {
                    EtcdKeysResponse response = getEtcdClient().put(candidatePath, m_identity).prevExist(true)
                        .ttl(getCandidateTTL()).timeout(getRequestTimeout(), SECONDS).send().get();
                    m_updateCandidatePathFuture =
                        getExecutor().schedule(this, getCandidateTTL() - getUpdateMargin(), SECONDS);
                    log(LOG_DEBUG, "%s: Updated candidate path - topic=%s candidate=%s key=%s mod=%s ttl=%s",
                        getName(), m_topic, m_identity, response.node.key, getModified(response), getCandidateTTL());
                }
            }
            catch (Exception e) {
                log(LOG_WARNING, "%s: Failed to candidate leader path - topic=%s candidate=%s! Resetting...", e,
                    getName(), m_topic, m_identity);
                cancel(m_initFuture, m_updateElectionPathFuture, m_updateCandidatePathFuture);
                clearMembership();
                m_candidatePath = null;
                m_initFuture = getExecutor().schedule(new InitTask(), getRetryDelay(), SECONDS);
            }
        }
    }
}
