/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.CANDIDATE_ADDED;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.ELECTION_ADDED;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.LEADER_CHANGED;
import static org.amdatu.etcd.util.EtcdUtil.cancel;
import static org.amdatu.etcd.util.EtcdUtil.getModified;
import static org.amdatu.etcd.util.EtcdUtil.isUpdateAction;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_ERROR;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.amdatu.etcd.locking.ElectionEvent;
import org.amdatu.etcd.locking.ElectionEvent.TYPE;
import org.amdatu.etcd.locking.ElectionListener;

import mousio.client.promises.ResponsePromise;
import mousio.client.promises.ResponsePromise.IsSimplePromiseResponseHandler;
import mousio.etcd4j.promises.EtcdResponsePromise;
import mousio.etcd4j.responses.EtcdException;
import mousio.etcd4j.responses.EtcdKeysResponse;
import mousio.etcd4j.responses.EtcdKeysResponse.EtcdNode;

/**
 * An internal delegate that monitors election topics.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ElectionsMonitor extends DelegateBase implements Runnable,
    IsSimplePromiseResponseHandler<EtcdKeysResponse> {

    private final Map<String, ElectionMonitor> m_topics = new HashMap<>();
    private final Set<ElectionListener> m_listeners = new HashSet<>();

    private volatile EtcdResponsePromise<EtcdKeysResponse> m_promise;
    private volatile ScheduledFuture<?> m_future;

    public ElectionsMonitor(ElectionComponent component) {
        super(component, ElectionsMonitor.class.getSimpleName());
    }

    @Override
    public void startDelegate() {
        log(LOG_DEBUG, "%s: Starting", getName());
        getExecutor().execute(this);
    }

    @Override
    public void stopDelegate() {
        log(LOG_DEBUG, "%s: Stopping", getName());
        cancel(m_promise);
        cancel(m_future);
        clearTopics();
    }

    public Collection<String> getTopics() {
        synchronized (m_topics) {
            return new HashSet<>(m_topics.keySet());
        }
    }

    public String getLeader(String topic) {
        synchronized (m_topics) {
            for (ElectionMonitor monitor : m_topics.values()) {
                if (monitor.getTopic().equals(topic)) {
                    return monitor.getLeader();
                }
            }
        }
        return null;
    }

    public Collection<String> getCandidates(String topic) {
        synchronized (m_topics) {
            for (ElectionMonitor monitor : m_topics.values()) {
                if (monitor.getTopic().equals(topic)) {
                    return monitor.getCandidates();
                }
            }
        }
        return null;
    }

    public void addElectionListener(ElectionListener listener) {
        synchronized (m_topics) {
            m_listeners.add(listener);
            for (ElectionMonitor monitor : m_topics.values()) {
                listener.handle(new ElectionEvent(ELECTION_ADDED, monitor.getTopic(), null));
                for (String candidate : monitor.getCandidates()) {
                    listener.handle(new ElectionEvent(CANDIDATE_ADDED, monitor.getTopic(), candidate));
                }
                listener.handle(new ElectionEvent(LEADER_CHANGED, monitor.getTopic(), monitor.getLeader()));
            }
        }
    }

    public void removeElectionListener(ElectionListener listener) {
        synchronized (m_topics) {
            m_listeners.remove(listener);
        }
    }

    @Override
    public void run() {
        if (!isActive()) {
            log(LOG_WARNING, "%s: Monitor inactive.. cancelling init task", getName());
            return;
        }
        try {
            EtcdKeysResponse response = getEtcdClient().putDir(getBasePath()).prevExist(false)
                .timeout(getRequestTimeout(), SECONDS).send().get();
            log(LOG_DEBUG, "%s: Created base path - key=%s mod=%s", getName(), response.node.key,
                getModified(response));
        }
        catch (EtcdException e) {
            if (e.errorCode != 105) {
                log(LOG_WARNING, "%s: Failed to create topics path - path=%s. Retrying...", getBasePath());
                m_future = getExecutor().schedule(this, getRetryDelay(), TimeUnit.SECONDS);
                return;
            }
            log(LOG_DEBUG, "%s: Base path exists - path=%s", getName(), getBasePath());
        }
        catch (Exception e) {
            log(LOG_WARNING, "Failed to base path - path=%s. Retrying...", e, getBasePath());
            m_future = getExecutor().schedule(this, getRetryDelay(), TimeUnit.SECONDS);
            return;
        }
        try {
            EtcdKeysResponse topicsResponse =
                getEtcdClient().getDir(getBasePath()).timeout(getRequestTimeout(), SECONDS).send().get();
            long topicsModified = getModified(topicsResponse);
            for (EtcdNode node : topicsResponse.node.nodes) {
                String topic = node.key.substring(getBasePath().length() + 1);
                ElectionMonitor monitor = addorUpdateTopic(topic, topicsModified);
                EtcdKeysResponse candidatesResponse =
                    getEtcdClient().getDir(getTopicPath(topic)).timeout(getRequestTimeout(), SECONDS).send().get();
                long candidatesModified = getModified(candidatesResponse);
                for (EtcdNode candidateNode : candidatesResponse.node.nodes) {
                    String key = candidateNode.key.substring(getTopicPath(topic).length() + 1);
                    monitor.addOrUpdateCandidate(key, candidateNode.value, candidatesModified);
                }
            }

            log(LOG_DEBUG, "Watching base path - key=%s mod=%s", getBasePath(), topicsModified + 1);
            m_promise = getEtcdClient().getDir(getBasePath()).recursive().waitForChange(topicsModified + 1).send();
            m_promise.addListener(this);
        }
        catch (Exception e) {
            log(LOG_WARNING, "Failed to query base path! Resetting...", e);
            clearTopics();
            m_future = getExecutor().schedule(this, getRetryDelay(), TimeUnit.SECONDS);
        }

    }

    @Override
    public void onResponse(ResponsePromise<EtcdKeysResponse> promise) {
        if (!isActive()) {
            log(LOG_WARNING, "%s: Monitor inactive.. cancelling response handling", getName());
            return;
        }
        if (promise.getException() != null && (promise.getException() instanceof CancellationException)) {
            log(LOG_DEBUG, "%s: Base watch cancelled - topic=%s", getName());
            return;
        }
        try {
            EtcdKeysResponse response = promise.get();
            long modified = getModified(response);
            log(LOG_DEBUG, "%s: Base watch response - key=%s mod=%s action=%s", getName(), response.node.key, modified,
                response.action);

            String subPath = getBasePath() + "/";
            if (!response.node.key.startsWith(subPath)) {
                log(LOG_WARNING, "%s: Basedir was updated! This is unexpected... resetting", getName());
                m_future = getExecutor().schedule(this, getRetryDelay(), TimeUnit.SECONDS);
                clearTopics();
                return;
            }

            String[] pathParts = response.node.key.replace(subPath, "").split("/");
            if (pathParts.length == 1) {
                if (isUpdateAction(response.action)) {
                    addorUpdateTopic(pathParts[0], modified);
                }
                else {
                    removeTopic(pathParts[0], modified);
                }
            }
            else if (pathParts.length == 2) {
                if (isUpdateAction(response.action)) {
                    addOrUpdateCandidate(pathParts[0], pathParts[1], response.node.value, modified);
                }
                else {
                    removeCandidate(pathParts[0], pathParts[1], response.node.value, modified);
                }
            }
            else {
                log(LOG_ERROR, "Unknown response key: %s", response.node.key);
            }

            log(LOG_DEBUG, "Watching base path - key=%s mod=%s", getBasePath(), modified + 1);
            m_promise = getEtcdClient().getDir(getBasePath()).recursive().waitForChange(modified + 1).send();
            m_promise.addListener(this);
        }
        catch (Exception e) {
            log(LOG_WARNING, "%s: Topics path watch failed! Resetting...", e, getName());
            clearTopics();
            m_future = getExecutor().schedule(this, getRetryDelay(), TimeUnit.SECONDS);
        }
    }

    private ElectionMonitor addorUpdateTopic(String key, long modified) {
        ElectionMonitor topicMonitor = null;
        boolean topicAdded = false;
        synchronized (m_topics) {
            topicMonitor = m_topics.get(key);
            if (topicMonitor == null) {
                topicMonitor = new ElectionMonitor(getComponent(), new ElectionMonitorAdapter(), key);
                m_topics.put(key, topicMonitor);
                topicAdded = true;
            }
        }
        if (topicAdded) {
            log(LOG_INFO, "%s: Added topic - topic=%s", getName(), key);
            invokeListeners(new ElectionEvent(TYPE.ELECTION_ADDED, key, null));
            try {
                topicMonitor.start();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return topicMonitor;
    }

    private void removeTopic(String key, long modified) {
        ElectionMonitor monitor = null;
        synchronized (m_topics) {
            monitor = m_topics.remove(key);
        }
        if (monitor != null) {
            log(LOG_INFO, "%s: Removed topic - topic=%s", getName(), key);
            invokeListeners(new ElectionEvent(TYPE.ELECTION_REMOVED, key, null));
            try {
                monitor.stop();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void addOrUpdateCandidate(String topic, String key, String value, long modified) {
        ElectionMonitor topicMonitor = null;
        boolean topicAdded = false;
        synchronized (m_topics) {
            topicMonitor = m_topics.get(topic);
            if (topicMonitor == null) {
                topicMonitor = new ElectionMonitor(getComponent(), new ElectionMonitorAdapter(), key);
                m_topics.put(key, topicMonitor);
                topicAdded = true;
            }
        }
        if (topicAdded) {
            log(LOG_WARNING, "%s: Added topic for candidate (topic was missing)- topic=%s key=%s value=%s", getName(),
                topic, key, value);
            invokeListeners(new ElectionEvent(TYPE.ELECTION_ADDED, key, null));
            try {
                topicMonitor.start();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        topicMonitor.addOrUpdateCandidate(key, value, modified);
    }

    private void removeCandidate(String topic, String key, String value, long modified) {
        ElectionMonitor monitor = null;
        synchronized (m_topics) {
            monitor = m_topics.get(topic);
        }
        if (monitor != null) {
            monitor.removeCandidate(key, value, modified);
        }
        else {
            log(LOG_WARNING, "%s: Can not remove candidate (monitor missing): topic= %s key=%s value=%s", getName(),
                topic, key, value);
        }
    }

    private void clearTopics() {
        synchronized (m_topics) {
            for (Entry<String, ElectionMonitor> topic : m_topics.entrySet()) {
                try {
                    topic.getValue().stop();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                ElectionEvent event = new ElectionEvent(TYPE.ELECTION_REMOVED, topic.getKey(), null);
                invokeListeners(event);
            }
            m_topics.clear();
        }
    }

    private void invokeListeners(ElectionEvent event) {
        synchronized (m_topics) {
            for (ElectionListener listener : m_listeners) {
                listener.handle(event);
            }
        }
    }

    private class ElectionMonitorAdapter implements ElectionListener {

        @Override
        public void handle(ElectionEvent event) {
            invokeListeners(event);
        }
    }
}
