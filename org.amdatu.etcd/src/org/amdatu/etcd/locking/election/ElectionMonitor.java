/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election;

import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_INFO;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.amdatu.etcd.locking.ElectionEvent;
import org.amdatu.etcd.locking.ElectionEvent.TYPE;
import org.amdatu.etcd.locking.ElectionListener;

/**
 * An internal delegate that monitors a single election.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ElectionMonitor extends DelegateBase {

    private final TreeMap<String, String> m_candidates = new TreeMap<>();
    private final ElectionListener m_listener;
    private final String m_topic;

    private volatile String m_leader;

    public ElectionMonitor(ElectionComponent component, ElectionListener listener, String topic) {
        super(component, ElectionMonitor.class.getSimpleName());
        m_topic = topic;
        m_listener = listener;
    }

    @Override
    public void startDelegate() {
        log(LOG_DEBUG, "%s: Starting - topic=%s", getName(), m_topic);
    }

    @Override
    public void stopDelegate() {
        log(LOG_DEBUG, "%s: Stopping - topic=%s", getName(), m_topic);
        synchronized (m_candidates) {
            clearCandidates();
        }
    }

    public Collection<String> getCandidates() {
        synchronized (m_candidates) {
            return new HashSet<>(m_candidates.values());
        }
    }

    public String getTopic() {
        return m_topic;
    }

    public String getLeader() {
        return m_leader;
    }

    public void addOrUpdateCandidate(String key, String value, long modified) {
        String previous = null;
        boolean changed = false;
        synchronized (m_candidates) {
            previous = m_candidates.put(key, value);
            if (previous == null) {
                changed = updateLeader();
            }
        }
        if (previous == null) {
            log(LOG_INFO, "%s: Added candidate: topic=%s identity=%s", getName(), m_topic, value);
            m_listener.handle(new ElectionEvent(TYPE.CANDIDATE_ADDED, m_topic, value));
            if (changed) {
                log(LOG_INFO, "%s: Leader changed - topic=%s leader=%s", getName(), m_topic, m_leader);
                m_listener.handle(new ElectionEvent(TYPE.LEADER_CHANGED, m_topic, m_leader));
            }
        }
    }

    public void removeCandidate(String key, String value, long modified) {
        boolean changed = false;
        synchronized (m_candidates) {
            value = m_candidates.remove(key);
            if (value != null) {
                changed = updateLeader();
            }
        }
        if (value != null) {
            log(LOG_INFO, "%s: Removed candidate: topic=%s identity=%s", getName(), m_topic, value);
            m_listener.handle(new ElectionEvent(TYPE.CANDIDATE_REMOVED, m_topic, value));
            if (changed) {
                log(LOG_INFO, "%s: Leader changed - topic=%s leader=%s", getName(), m_topic, m_leader);
                m_listener.handle(new ElectionEvent(TYPE.LEADER_CHANGED, m_topic, m_leader));
            }
        }
    }

    private boolean updateLeader() {
        String leader = m_candidates.isEmpty() ? null : m_candidates.firstEntry().getValue();
        if ((leader == null && m_leader != null) || (leader != null && !leader.equals(m_leader))) {
            m_leader = leader;
            return true;
        }
        return false;
    }

    private void clearCandidates() {
        for (Entry<String, String> entry : m_candidates.entrySet()) {
            m_listener.handle(new ElectionEvent(TYPE.CANDIDATE_REMOVED, m_topic, entry.getValue()));
        }
        m_candidates.clear();
    }
}
