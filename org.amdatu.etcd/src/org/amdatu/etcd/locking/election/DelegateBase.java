/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import mousio.etcd4j.EtcdClient;

/**
 * A base class for delegates to the Election Manager.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public abstract class DelegateBase {

    private final AtomicBoolean m_active = new AtomicBoolean(false);

    private final ElectionComponent m_component;
    private final String m_name;

    public DelegateBase(ElectionComponent component, String name) {
        m_component = component;
        m_name = name;
    }

    public void start() throws Exception {
        if (m_active.compareAndSet(false, true)) {
            startDelegate();
        }
    }

    public void stop() throws Exception {
        if (m_active.compareAndSet(true, false)) {
            stopDelegate();
        }
    }

    public abstract void startDelegate() throws Exception;

    public abstract void stopDelegate() throws Exception;

    protected boolean isActive() {
        return m_active.get();
    }

    protected String getName() {
        return m_name;
    }

    protected ElectionComponent getComponent() {
        return m_component;
    }

    protected EtcdClient getEtcdClient() {
        return m_component.getEtcdClient();
    }

    protected ScheduledExecutorService getExecutor() {
        return m_component.getExecutor();
    }

    protected String getTopicPath(String topic) {
        return m_component.getElectionPath(topic);
    }

    protected int getLeaderTTL() {
        return m_component.getLeaderTTL();
    }

    protected int getCandidateTTL() {
        return m_component.getCandidateTTL();
    }

    protected int getTopicTTL() {
        return m_component.getTopicTTL();
    }

    protected int getUpdateMargin() {
        return m_component.getUpdateMargin();
    }

    protected int getRetryDelay() {
        return m_component.getRetryDelay();
    }

    protected int getRequestTimeout() {
        return m_component.getRequestTimeout();
    }

    protected String getBasePath() {
        return m_component.getBasePath();
    }

    protected void log(int lvl, String msg, Object... args) {
        m_component.log(lvl, msg, args);
    }

    protected void log(int lvl, String msg, Throwable t, Object... args) {
        m_component.log(lvl, msg, t, args);
    }
}
