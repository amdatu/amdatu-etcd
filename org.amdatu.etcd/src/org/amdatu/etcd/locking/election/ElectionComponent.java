/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election;

import static java.lang.String.format;
import static org.amdatu.etcd.locking.ElectionCandidate.ELECTION_IDENTITY;
import static org.amdatu.etcd.locking.ElectionCandidate.ELECTION_TOPIC;
import static org.amdatu.etcd.util.ConfigurationUtil.getIntegerProperty;
import static org.amdatu.etcd.util.ConfigurationUtil.getStringPlusProperty;
import static org.amdatu.etcd.util.ConfigurationUtil.getStringProperty;
import static org.amdatu.etcd.util.ConfigurationUtil.getUriPlusProperty;
import static org.amdatu.etcd.util.EtcdUtil.getStringProperty;
import static org.amdatu.etcd.util.EtcdUtil.isEmpty;
import static org.amdatu.etcd.util.EtcdUtil.isSafeTopic;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.amdatu.etcd.locking.ElectionCandidate;
import org.amdatu.etcd.locking.ElectionListener;
import org.amdatu.etcd.locking.ElectionManager;
import org.amdatu.etcd.util.EtcdUtil;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ConfigurationDependency;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.log.LogService;

import mousio.etcd4j.EtcdClient;

/**
 * Main election component.
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Component
public class ElectionComponent implements ElectionManager {

    /** Configuration PID */
    public final static String ELECTION_CONFIG_PID = "org.amdatu.etcd.locking.election";

    /** Configuration property for the ETCD endpoint. */
    public final static String ELECTION_CONFIG_ETCDENDPOINTS = "etcdEndpoints";
    public final static String ELECTION_CONFIG_ETCDENDPOINTS_DEFAULT = "http://127.0.0.1:2379/v2";

    /** Configuration property for the topic TTL in seconds. */
    public final static String ELECTION_CONFIG_TOPICTTL = "topicTtl";
    public final static String ELECTION_CONFIG_TOPICTTL_DEFAULT = "300";

    /** Configuration property for the leader TTL in seconds. */
    public final static String ELECTION_CONFIG_LEADERTTL = "leaderTTL";
    public final static String ELECTION_CONFIG_LEADERTTL_DEFAULT = "30";

    /** Configuration property for the candidate TTL in seconds. */
    public final static String ELECTION_CONFIG_CANDIDATETTL = "candidateTTL";
    public final static String ELECTION_CONFIG_CANDIDATETTL_DEFAULT = "60";

    /** Configuration property for the margin to use for TTL updates in seconds. */
    public final static String ELECTION_CONFIG_UPDATEMARGIN = "updateMargin";
    public final static String ELECTION_CONFIG_UPDATEMARGIN_DEFAULT = "2";

    /** Configuration property for the failure retry interval in seconds. */
    public final static String ELECTION_CONFIG_RETRYDELAY = "retryDelay";
    public final static String ELECTION_CONFIG_RETRYDELAY_DEFAULT = "1";

    /** Configuration property for the request timeout in seconds. */
    public final static String ELECTION_CONFIG_REQUESTTIMEOUT = "requestTimeout";
    public final static String ELECTION_CONFIG_REQUESTTIMEOUT_DEFAULT = "1";

    /** Configuration property for the base path etc ETCD */
    public final static String ELECTION_CONFIG_BASEPATH = "basePath";
    public final static String ELECTION_CONFIG_BASEPATH_DEFAULT = "/org.amdatu.etcd/locking/election";

    /**
     * Configuration property for debug logging. Boolean value defaults to false
     */
    public final static String ELECTION_CONFIG_DEBUG = "logDebug";

    private final Map<ServiceReference<?>, CandidateHandler> m_candidates = new HashMap<>();
    private final Map<ServiceReference<?>, ListenerHandler> m_listeners = new HashMap<>();
    private final Object m_lock = new Object();

    private final AtomicBoolean m_active = new AtomicBoolean(false);

    @ServiceDependency(required = false)
    private volatile LogService m_log;

    private volatile ElectionsMonitor m_monitor;
    private volatile ScheduledExecutorService m_executor;
    private volatile EtcdClient m_client;

    private volatile String[] m_etcdEndpoints;
    private volatile URI[] m_etcdEndpointURIs;
    private volatile String m_basePath;
    private volatile int m_leaderTtl;
    private volatile int m_topicTtl;
    private volatile int m_candidateTtl;
    private volatile int m_updateMargin;
    private volatile int m_retryDelay;
    private volatile int m_requestTimeout;
    private volatile boolean m_debug;

    @ConfigurationDependency(pid = ELECTION_CONFIG_PID)
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        if (properties == null) {
            stopHandlers();
            return;
        }
        String[] etcdEndpoints = getStringPlusProperty(properties, ELECTION_CONFIG_ETCDENDPOINTS,
            ELECTION_CONFIG_ETCDENDPOINTS_DEFAULT);
        URI[] etcdEndpointURIs = getUriPlusProperty(properties, ELECTION_CONFIG_ETCDENDPOINTS,
            ELECTION_CONFIG_ETCDENDPOINTS_DEFAULT);
        int topicTTL = getIntegerProperty(properties, ELECTION_CONFIG_TOPICTTL, ELECTION_CONFIG_TOPICTTL_DEFAULT);
        int leaderTTL = getIntegerProperty(properties, ELECTION_CONFIG_LEADERTTL, ELECTION_CONFIG_LEADERTTL_DEFAULT);
        int candidateTTL = getIntegerProperty(properties, ELECTION_CONFIG_CANDIDATETTL,
            ELECTION_CONFIG_CANDIDATETTL_DEFAULT);
        int updateMargin = getIntegerProperty(properties, ELECTION_CONFIG_UPDATEMARGIN,
            ELECTION_CONFIG_UPDATEMARGIN_DEFAULT);
        int retryDelay = getIntegerProperty(properties, ELECTION_CONFIG_RETRYDELAY, ELECTION_CONFIG_RETRYDELAY_DEFAULT);
        int requestTimeout =
            getIntegerProperty(properties, ELECTION_CONFIG_REQUESTTIMEOUT, ELECTION_CONFIG_REQUESTTIMEOUT_DEFAULT);

        String basePath = getStringProperty(properties, ELECTION_CONFIG_BASEPATH, ELECTION_CONFIG_BASEPATH_DEFAULT);
        boolean debug = Boolean.valueOf(getStringProperty(properties, ELECTION_CONFIG_DEBUG, "false"));

        boolean restartHandlers = false;
        if (m_etcdEndpoints == null || !(Arrays.equals(m_etcdEndpoints, etcdEndpoints))) {
            restartHandlers = true;
        }
        if (isEmpty(m_basePath) || !m_basePath.equals(basePath)) {
            restartHandlers = true;
        }
        m_etcdEndpoints = etcdEndpoints;
        m_etcdEndpointURIs = etcdEndpointURIs;
        m_basePath = basePath;
        m_topicTtl = topicTTL;
        m_leaderTtl = leaderTTL;
        m_candidateTtl = candidateTTL;
        m_updateMargin = updateMargin;
        m_retryDelay = retryDelay;
        m_requestTimeout = requestTimeout;
        m_debug = debug;
        if (restartHandlers) {
            restartHandlers();
        }
    }

    @Start
    public void start() throws Exception {
        log(LOG_DEBUG, "%s: Starting", getName());
        m_executor = Executors.newSingleThreadScheduledExecutor();
        m_client = new EtcdClient(m_etcdEndpointURIs);
        m_monitor = new ElectionsMonitor(this);
        m_monitor.start();
        startHandlers();
        m_active.set(true);
    }

    @Stop
    public void stop() throws Exception {
        log(LOG_DEBUG, "%s: Stopping", getName());
        m_active.set(false);
        stopHandlers();
        m_monitor.stop();
        m_client.close();
        m_executor.shutdownNow();
    }

    @Override
    public Collection<String> getTopics() {
        return m_monitor.getTopics();
    }

    @Override
    public String getLeader(String topic) {
        return m_monitor.getLeader(topic);
    }

    @Override
    public Collection<String> getCandidates(String topic) {
        return m_monitor.getCandidates(topic);
    }

    @ServiceDependency(required = false, removed = "removeElectionCandidate", changed = "changeElectionCandidate")
    public void addElectionCandidate(ServiceReference<?> reference, ElectionCandidate candidate) {
        String topic = EtcdUtil.getStringProperty(reference, ELECTION_TOPIC);
        if (isEmpty(topic) || !isSafeTopic(topic)) {
            log(LOG_WARNING, "%s: Ignoring candidate registration with empty or unsafe topic - value=%s service=%s",
                getName(), topic, reference);
            return;
        }
        String identity = getStringProperty(reference, ELECTION_IDENTITY);
        if (isEmpty(identity)) {
            log(LOG_WARNING, "Ignoring candidate registration without identity - value=%s service=%s", getName(),
                topic, reference);
            return;
        }
        CandidateHandler handler = new CandidateHandler(this, candidate, topic, identity);
        synchronized (m_lock) {
            for (CandidateHandler test : m_candidates.values()) {
                if (test.getTopic().equals(topic) && test.getIdentity().equals(identity)) {
                    log(LOG_WARNING, "%s: Ignoring candidate registration with duplicate topic/identity: %s",
                        getName(), reference);
                    return;
                }
            }
            m_candidates.put(reference, handler);
            if (m_active.get()) {
                m_monitor.addElectionListener((ElectionListener) handler);
                startDelegate(handler);
            }
        }
    }

    public void changeElectionCandidate(ServiceReference<?> reference, ElectionCandidate candidate) {
        removeElectionCandidate(reference);
        addElectionCandidate(reference, candidate);
    }

    public void removeElectionCandidate(ServiceReference<?> reference) {
        CandidateHandler handler = null;
        synchronized (m_lock) {
            handler = m_candidates.remove(reference);
            if (handler != null) {
                m_monitor.removeElectionListener((ElectionListener) handler);
                stopDelegate(handler);
            }
        }
    }

    @ServiceDependency(required = false, removed = "removeElectionListener", changed = "changeElectionListener")
    public void addElectionListener(ServiceReference<?> reference, ElectionListener listener) {
        String topic = getStringProperty(reference, ELECTION_TOPIC);
        if (!isEmpty(topic) && !isSafeTopic(topic)) {
            log(LOG_WARNING, "%s: Ignoring listener registration with unsafe topic - value=%s service=%s", getName(),
                topic, reference);
            return;
        }
        ListenerHandler handler = new ListenerHandler(this, listener, topic);
        synchronized (m_lock) {
            m_listeners.put(reference, handler);
            if (m_active.get()) {
                startDelegate(handler);
                m_monitor.addElectionListener(handler);
            }
        }
    }

    public void changeElectionListener(ServiceReference<?> reference, ElectionListener candidate) {
        removeElectionListener(reference);
        addElectionListener(reference, candidate);
    }

    public void removeElectionListener(ServiceReference<?> reference) {
        ListenerHandler handler = null;
        synchronized (m_lock) {
            handler = m_listeners.remove(reference);
            if (handler != null) {
                m_monitor.removeElectionListener(handler);
                stopDelegate(handler);
            }
        }
    }

    protected EtcdClient getEtcdClient() {
        return m_client;
    }

    protected ScheduledExecutorService getExecutor() {
        return m_executor;
    }

    protected int getLeaderTTL() {
        return m_leaderTtl;
    }

    protected int getCandidateTTL() {
        return m_candidateTtl;
    }

    protected int getTopicTTL() {
        return m_topicTtl;
    }

    protected int getUpdateMargin() {
        return m_updateMargin;
    }

    protected int getRetryDelay() {
        return m_retryDelay;
    }

    protected int getRequestTimeout() {
        return m_requestTimeout;
    }

    protected String getBasePath() {
        return m_basePath;
    }

    protected String getElectionPath(String topic) {
        return getBasePath() + "/" + topic;
    }

    protected void log(int lvl, String msg, Object... args) {
        log(lvl, msg, null, args);
    }

    protected void log(int lvl, String msg, Throwable t, Object... args) {
        boolean debug = m_debug;
        if (!debug && lvl >= LOG_DEBUG) {
            return;
        }
        LogService log = m_log;
        if (log == null) {
            return;
        }
        try {
            String message = format(msg, args);
            log.log(lvl, message, t);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getName() {
        return ElectionComponent.class.getSimpleName();
    }

    private void restartHandlers() {
        synchronized (m_lock) {
            for (CandidateHandler handler : m_candidates.values()) {
                stopDelegate(handler);
                startDelegate(handler);
            }
        }
    }

    private void startHandlers() {
        synchronized (m_lock) {
            for (CandidateHandler handler : m_candidates.values()) {
                m_monitor.addElectionListener((ElectionListener) handler);
                startDelegate(handler);
            }
            for (ListenerHandler handler : m_listeners.values()) {
                m_monitor.addElectionListener((ElectionListener) handler);
                startDelegate(handler);
            }
        }
    }

    private void stopHandlers() {
        synchronized (m_lock) {
            for (CandidateHandler handler : m_candidates.values()) {
                m_monitor.removeElectionListener((ElectionListener) handler);
                stopDelegate(handler);
            }
            for (ListenerHandler handler : m_listeners.values()) {
                m_monitor.removeElectionListener((ElectionListener) handler);
                stopDelegate(handler);
            }
        }
    }

    private void startDelegate(DelegateBase delegate) {
        if (delegate == null) {
            return;
        }
        try {
            delegate.start();
        }
        catch (Exception e) {
            log(LOG_DEBUG, "Failed to start delegate", e);
        }
    }

    private void stopDelegate(DelegateBase delegate) {
        if (delegate == null) {
            return;
        }
        try {
            delegate.stop();
        }
        catch (Exception e) {
            log(LOG_DEBUG, "Failed to stop delegate", e);
        }
    }
}
