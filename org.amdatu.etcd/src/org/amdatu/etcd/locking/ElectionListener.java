/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking;

/**
 * A white board service that represents a listener for events.
 * <p/>
 * An Election Listener represents a participant in the distributed model that
 * is interested in Election Events.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface ElectionListener {

    /**
     * Service Registration property key for Election Listener services. The
     * optional topic identifies a logical election that it is interested in. If
     * no topic is provided it will receive all Election Events.
     */
    String ELECTION_TOPIC = "org.amdatu.etcd.locking.election.topic";

    /**
     * Callback that informs the listener of an Election Event.
     * 
     * @param event
     *        The event
     */
    void handle(ElectionEvent event);
}