/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking;

/**
 * A white board service that represents a candidate in an election.
 * <p/>
 * An Election Candidate represents a participant in the distributed model that
 * takes part in a logical election.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface ElectionCandidate {

    /**
     * Registration property key for Election Candidate services. The topic
     * identifies a logical election.
     */
    String ELECTION_TOPIC = "org.amdatu.etcd.locking.election.topic";

    /**
     * Registration property key for Election Candidate services. The identity
     * uniquely identifies the candidate.
     */
    String ELECTION_IDENTITY = "org.amdatu.etcd.locking.election.identity";

    /**
     * Callback that informs the candidate that it has become the leader.
     */
    void elected();

    /**
     * Callback that informs the candidate that it is no longer the leader.
     */
    void demoted();
}
