/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking;

/**
 * An Election Event.
 * <p/> {@code ElectionEvent} objects are delivered to all registered {@link ElectionListener} services where the topic matches the {@link ElectionListener#ELECTION_TOPIC} registration property of the Election
 * Listener.
 * <p/>
 * A type enum is used to identify the type of event. The following event types
 * are defined:
 * <ul>
 * <li>{@link TYPE#ELECTION_ADDED}</li>
 * <li>{@link TYPE#ELECTION_REMOVED}</li>
 * <li>{@link TYPE#CANDIDATE_ADDED}</li>
 * <li>{@link TYPE#CANDIDATE_REMOVED}</li>
 * <li>{@link TYPE#LEADER_CHANGED}</li>
 * </ul>
 * Additional event types may be defined in the future.
 * <p/>
 * 
 * @see ElectionListener
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ElectionEvent {

    public enum TYPE {
            ELECTION_ADDED, ELECTION_REMOVED, CANDIDATE_ADDED, CANDIDATE_REMOVED, LEADER_CHANGED
    }

    private final TYPE m_type;
    private final String m_topic;
    private final String m_identity;

    public ElectionEvent(TYPE type, String topic, String identity) {
        m_type = type;
        m_topic = topic;
        m_identity = identity;
    }

    /**
     * Return the type of this event.
     * 
     * @return The type of this event.
     */
    public TYPE getType() {
        return m_type;
    }

    /**
     * Return the election topic of this event.
     * 
     * @return The topic of this event. May be {@code null}.
     */
    public String getTopic() {
        return m_topic;
    }

    /**
     * Return the candidate identity of this event.
     * 
     * @return The identity of this event. May be {@code null}.
     */
    public String getIdentity() {
        return m_identity;
    }

    @Override
    public String toString() {
        return String.format("ElectionEvent{ type=%s, topic=%s, identity=%s}", m_type, m_topic, m_identity);
    }
}
