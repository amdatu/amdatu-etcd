/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking;

import java.util.Collection;

/**
 * A service that represents the Election Manager.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public interface ElectionManager {

    /**
     * Return the election topics
     * 
     * @return The election topics.
     */
    Collection<String> getTopics();

    /**
     * Return the leader for a specific topic.
     * 
     * @return The election topics. May be {@code null}.
     */
    String getLeader(String topic);

    /**
     * Return the candidate for a specific topic.
     * 
     * @return The election topics.
     */
    Collection<String> getCandidates(String topic);
}
