/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.configurator.impl;

import static java.lang.String.format;
import static java.lang.String.join;
import static org.amdatu.etcd.util.ConfigurationUtil.getStringPlusProperty;
import static org.amdatu.etcd.util.ConfigurationUtil.getStringProperty;
import static org.amdatu.etcd.util.ConfigurationUtil.getUriPlusProperty;
import static org.amdatu.etcd.util.EtcdUtil.isEmpty;
import static org.amdatu.etcd.util.EtcdUtil.toDictionary;
import static org.amdatu.etcd.util.EtcdUtil.toJson;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.net.URI;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ConfigurationDependency;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.log.LogService;

import mousio.etcd4j.EtcdClient;

/**
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Component
public class ConfiguratorComponent {

    /** Configuration PID */
    public final static String CONFIGURATOR_CONFIG_PID = "org.amdatu.etcd.configurator";

    /** PID storage key */
    public final static String CONFIGURATOR_PID_KEY = "etcd.configurator.pid";

    /** Configuration property for the ETCD endpoints. */
    public final static String CONFIGURATOR_CONFIG_ETCDENDPOINTS = "etcdEndpoints";
    public final static String CONFIGURATOR_CONFIG_ETCDENDPOINTS_DEFAULT = "http://127.0.0.1:2379/v2";

    /** Configuration property for the etcd path in ETCD */
    public final static String CONFIGURATOR_CONFIG_BASEPATH = "basePath";
    public final static String CONFIGURATOR_CONFIG_BASEPATH_DEFAULT = "/org.amdatu.etcd/configurator";

    /** Configuration flag for debug logging */
    public final static String CONFIGURATOR_CONFIG_DEBUGLOGGING = "debugLogging";
    public final static String CONFIGURATOR_CONFIG_DEBUGLOGGING_DEFAULT = "false";

    /** Configuration flag for console logging */
    public final static String CONFIGURATOR_CONFIG_CONSOLELOGGING = "consoleLogging";
    public final static String CONFIGURATOR_CONFIG_CONSOLELOGGING_DEFAULT = "false";

    private final Map<ServiceReference<?>, ConfigurationAdmin> m_configAdmins = new HashMap<>();
    private final Map<String, String> m_configSets = new HashMap<>();
    private final AtomicBoolean m_started = new AtomicBoolean();
    private final Object m_lock = new Object();

    @Inject
    private volatile BundleContext m_context;

    @ServiceDependency(required = false)
    private volatile LogService m_logService;

    private volatile EtcdClient m_etcdClient;
    private volatile ConfigurationMonitor m_configMonitor;
    private volatile boolean m_debugLogging = false;
    private volatile boolean m_consoleLogging = false;

    private volatile String[] m_etcdEndpoints;
    private volatile URI[] m_etcdEndpointURIs;
    private volatile String m_configPath;

    @ConfigurationDependency(pid = CONFIGURATOR_CONFIG_PID)
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        if (properties == null) {
            log(LOG_DEBUG, "Ignoring configuration update: properties=null");
            return;
        }
        log(LOG_DEBUG, "Processing configuration update: properties=%s", properties);
        boolean debugLogging = Boolean.valueOf(getStringProperty(properties, CONFIGURATOR_CONFIG_DEBUGLOGGING,
            CONFIGURATOR_CONFIG_DEBUGLOGGING_DEFAULT));
        boolean consoleLogging = Boolean.valueOf(getStringProperty(properties, CONFIGURATOR_CONFIG_CONSOLELOGGING,
            CONFIGURATOR_CONFIG_CONSOLELOGGING_DEFAULT));

        String[] etcdEndpoints = getStringPlusProperty(properties, CONFIGURATOR_CONFIG_ETCDENDPOINTS,
            CONFIGURATOR_CONFIG_ETCDENDPOINTS_DEFAULT);
        URI[] etcdEndpointURIs = getUriPlusProperty(properties, CONFIGURATOR_CONFIG_ETCDENDPOINTS,
            CONFIGURATOR_CONFIG_ETCDENDPOINTS_DEFAULT);
        String configPath = getStringProperty(properties, CONFIGURATOR_CONFIG_BASEPATH,
            CONFIGURATOR_CONFIG_BASEPATH_DEFAULT);
        synchronized (m_lock) {
            boolean restartRequired = false;
            m_debugLogging = debugLogging;
            m_consoleLogging = consoleLogging;
            if (m_etcdEndpoints == null || !(Arrays.equals(m_etcdEndpoints, etcdEndpoints))) {
                log(LOG_DEBUG, " * etcdEndpoints: %s => %s (restart required)", m_etcdEndpoints == null ? "null"
                    : join(",", m_etcdEndpoints), join(",", etcdEndpoints));
                m_etcdEndpoints = etcdEndpoints;
                m_etcdEndpointURIs = etcdEndpointURIs;
                restartRequired = true;
            }
            if (isEmpty(m_configPath) || !m_configPath.equals(configPath)) {
                log(LOG_DEBUG, " * configPath: %s => %s (restart required)", m_configPath, configPath);
                m_configPath = configPath;
                restartRequired = true;
            }
            if (m_started.get() && restartRequired) {
                log(LogService.LOG_DEBUG, "Configurator: restarting");
                internalStop();
                internalStart();
                log(LogService.LOG_DEBUG, "Configurator: restarted");
            }
        }
    }

    @Start
    public void start() throws Exception {
        synchronized (m_lock) {
            assert !m_started.get();
            log(LogService.LOG_DEBUG, "Configurator: starting");
            internalStart();
            m_started.set(true);
            log(LogService.LOG_DEBUG, "Configurator: started");
        }
    }

    @Stop
    public void stop() throws Exception {
        synchronized (m_lock) {
            assert m_started.get();
            log(LogService.LOG_DEBUG, "Configurator stopping");
            internalStop();
            log(LogService.LOG_DEBUG, "Configurator stopped");
        }
    }

    @ServiceDependency(removed = "remConfigurationAdmin", required = false)
    public void addConfigurationAdmin(ServiceReference<?> reference, ConfigurationAdmin admin) {
        synchronized (m_lock) {
            m_configAdmins.put(reference, admin);
            deleteConfigurations(admin);
            for (Entry<String, String> entry : m_configSets.entrySet()) {
                addOrUpdateConfiguration(admin, entry.getKey(), entry.getValue());
            }
        }
    }

    public void remConfigurationAdmin(ServiceReference<?> reference) {
        synchronized (m_lock) {
            ConfigurationAdmin admin = m_configAdmins.remove(reference);
            if (admin != null) {
                deleteConfigurations(admin);
            }
        }
    }

    protected EtcdClient getEtcdClient() {
        return m_etcdClient;
    }

    protected void log(int lvl, String msg, Object... args) {
        log(lvl, msg, null, args);
    }

    protected void log(int lvl, String msg, Throwable t, Object... args) {
        if (!m_debugLogging && lvl >= LOG_DEBUG) {
            return;
        }
        try {
            String logMessage = format(msg, args);
            LogService logService = m_logService;
            if (logService != null) {
                logService.log(lvl, logMessage, t);
            }
            if (m_consoleLogging) {
                System.out.println(logMessage);
                if (t != null) {
                    t.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void addOrUpdateConfiguration(String pid, String value) {
        synchronized (m_lock) {
            m_configSets.put(pid, value);
            for (ConfigurationAdmin admin : m_configAdmins.values()) {
                addOrUpdateConfiguration(admin, pid, value);
            }
        }
    }

    protected void deleteConfiguration(String pid) {
        synchronized (m_lock) {
            log(LOG_DEBUG, "executing configuration delete: %s", pid);
            m_configSets.remove(pid);
            for (ConfigurationAdmin admin : m_configAdmins.values()) {
                deleteConfiguration(admin, pid);
            }
        }
    }

    private void internalStart() {
        assert !m_started.get();
        try {
            m_etcdClient = new EtcdClient(m_etcdEndpointURIs);
            m_configMonitor = new ConfigurationMonitor(ConfiguratorComponent.this, m_configPath);
            m_started.set(true);
            m_configMonitor.start();
        }
        catch (Exception e) {
            log(LOG_WARNING, "Exception during async start", e);
        }
    }

    private void internalStop() {
        assert m_started.get();
        try {
            m_started.set(false);
            m_configMonitor.stop();
            m_configSets.clear();
            for (ConfigurationAdmin admin : m_configAdmins.values()) {
                deleteConfigurations(admin);
            }
            m_etcdClient.close();
        }
        catch (Exception e) {
            log(LOG_WARNING, "Exception during async stop", e);
        }
    }

    private void addOrUpdateConfiguration(ConfigurationAdmin admin, String pid, String value) {
        try {
            Dictionary<String, Object> properties = toDictionary(value);
            properties.put(CONFIGURATOR_PID_KEY, pid);
            Configuration[] configurations = admin.listConfigurations(format("(%s=%s)", CONFIGURATOR_PID_KEY, pid));
            if (configurations != null) {
                for (Configuration configuration : configurations) {
                    if (configuration.getProperties() != null) {
                        String existingValue = toJson(configuration.getProperties());
                        if (!existingValue.equals(value)) {
                            configuration.update(properties);
                        }
                    }
                }
            }
            else {
                Configuration configuration = null;
                int dashIndex = pid.indexOf("-");
                if (dashIndex != -1) {
                    configuration = admin.createFactoryConfiguration(pid.substring(0, dashIndex), null);
                }
                else {
                    configuration = admin.getConfiguration(pid, null);
                }
                configuration.update(properties);
            }
        }
        catch (Exception e) {
            log(LOG_WARNING, "Exception while updating configuration with pid=%s in admin=%s", e, pid, admin);
        }
    }

    private void deleteConfiguration(ConfigurationAdmin admin, String pid) {
        try {
            Configuration[] configurations = admin.listConfigurations(format("(%s=%s)", CONFIGURATOR_PID_KEY, pid));
            if (configurations != null) {
                for (Configuration configuration : configurations) {
                    configuration.delete();
                }
            }
        }
        catch (Exception e) {
            log(LOG_WARNING, "Exception while listing configuration with pid=%s from admin=%s", e, pid, admin);
        }
    }

    private void deleteConfigurations(ConfigurationAdmin admin) {
        try {
            Configuration[] configurations = admin.listConfigurations(format("(%s=*)", CONFIGURATOR_PID_KEY));
            if (configurations != null) {
                for (Configuration configuration : configurations) {
                    configuration.delete();
                }
            }
        }
        catch (Exception e) {
            log(LOG_WARNING, "Exception while deleting configurations from admin: %s", e, admin);
        }
    }
}
