/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.configurator.impl;

import static org.amdatu.etcd.util.EtcdUtil.cancel;
import static org.amdatu.etcd.util.EtcdUtil.getModified;
import static org.amdatu.etcd.util.EtcdUtil.isUpdateAction;
import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import mousio.client.promises.ResponsePromise;
import mousio.client.promises.ResponsePromise.IsSimplePromiseResponseHandler;
import mousio.etcd4j.EtcdClient;
import mousio.etcd4j.promises.EtcdResponsePromise;
import mousio.etcd4j.responses.EtcdException;
import mousio.etcd4j.responses.EtcdKeyAction;
import mousio.etcd4j.responses.EtcdKeysResponse;
import mousio.etcd4j.responses.EtcdKeysResponse.EtcdNode;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ConfigurationMonitor implements Runnable, IsSimplePromiseResponseHandler<EtcdKeysResponse> {

    public final static int RETRY_DELAY = 1;

    private final AtomicBoolean m_active = new AtomicBoolean();
    private final ConfiguratorComponent m_component;
    private final String m_path;

    private volatile ScheduledExecutorService m_executor;
    private volatile EtcdResponsePromise<EtcdKeysResponse> m_promise;
    private volatile ScheduledFuture<?> m_future;

    public ConfigurationMonitor(ConfiguratorComponent component, String path) {
        m_component = component;
        m_path = path;
    }

    public ConfiguratorComponent getComponent() {
        return m_component;
    }

    public boolean isActive() {
        return m_active.get();
    }

    public String getPath() {
        return m_path;
    }

    public EtcdClient getEtcdClient() {
        return m_component.getEtcdClient();
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

    public void start() {
        assert !m_active.get();
        getComponent().log(LOG_DEBUG, "%s: Starting", getName());
        scheduleReSet();
        m_active.set(true);
    }

    public void stop() {
        assert m_active.get();
        m_active.set(false);
        getComponent().log(LOG_DEBUG, "%s: Stopping", getName());
        cancel(m_promise);
        cancel(m_future);
    }

    @Override
    public void run() {
        if (!isActive()) {
            getComponent().log(LOG_WARNING, "%s: Monitor inactive.. cancelling init task", getName());
            return;
        }
        try {
            EtcdKeysResponse response = getEtcdClient().putDir(getPath()).prevExist(false).send().get();
            getComponent().log(LOG_DEBUG, "%s: Created base path - key=%s mod=%s", getName(), response.node.key,
                getModified(response));
        }
        catch (EtcdException e) {
            if (e.errorCode != 105) {
                getComponent().log(LOG_WARNING, "%s: Failed to create base path - Path=%s. Retrying...", getPath());
                scheduleReSet();
                return;
            }
            getComponent().log(LOG_DEBUG, "%s: Base path exists - path=%s", getName(), getPath());
        }
        catch (Exception e) {
            getComponent().log(LOG_WARNING, "Failed to create base path - Path=%s. Retrying...", e, getPath());
            scheduleReSet();
            return;
        }
        try {
            EtcdKeysResponse response = getEtcdClient().getDir(getPath()).send().get();
            long topicsModified = getModified(response);
            for (EtcdNode node : response.node.nodes) {
                if (node.dir) {
                    continue;
                }
                String pid = node.key.substring(getPath().length() + 1);
                getComponent().addOrUpdateConfiguration(pid, node.value);
            }
            getComponent().log(LOG_DEBUG, "Watching base path - key=%s mod=%s", getPath(), topicsModified + 1);
            m_promise = getEtcdClient().getDir(getPath()).recursive().waitForChange(topicsModified + 1).send();
            m_promise.addListener(this);
            m_executor.shutdown();
        }
        catch (Exception e) {
            getComponent().log(LOG_WARNING, "Failed to query base path! Resetting...", e);
            scheduleReSet();
        }
    }

    @Override
    public void onResponse(ResponsePromise<EtcdKeysResponse> promise) {
        if (!isActive()) {
            getComponent().log(LOG_WARNING, "%s: Monitor inactive.. cancelling response handling", getName());
            return;
        }
        if (promise.getException() != null && (promise.getException() instanceof CancellationException)) {
            getComponent().log(LOG_DEBUG, "%s: Base watch cancelled - topic=%s", getName());
            return;
        }
        try {
            EtcdKeysResponse response = promise.get();
            EtcdKeyAction action = response.action;
            EtcdNode node = response.node;
            long modified = getModified(response);
            getComponent().log(LOG_DEBUG, "%s: Base watch response - key=%s mod=%s action=%s", getName(), node.key,
                modified, action);
            if (node.dir) {
                getComponent().log(LOG_DEBUG, "%s: Base watch response - key=%s mod=%s action=%s", getName(), node.key,
                    modified, action);
            }
            else if (!node.key.startsWith(getPath() + "/")) {
                getComponent().log(LOG_DEBUG, "%s: Base watch response - key=%s mod=%s action=%s", getName(), node.key,
                    modified, action);
            }
            else {
                String pid = node.key.substring(getPath().length() + 1);
                if (isUpdateAction(action)) {
                    getComponent().addOrUpdateConfiguration(pid, node.value);
                }
                else {
                    getComponent().deleteConfiguration(pid);
                }
            }
            getComponent().log(LOG_DEBUG, "Watching base path - key=%s mod=%s", getPath(), modified + 1);
            m_promise = getEtcdClient().getDir(getPath()).recursive().waitForChange(modified + 1).send();
            m_promise.addListener(this);
        }
        catch (Exception e) {
            getComponent().log(LOG_WARNING, "%s: Topics path watch failed! Resetting...", e, getName());
            scheduleReSet();
        }
    }

    private void scheduleReSet() {
        if (m_executor == null) {
            m_executor = Executors.newSingleThreadScheduledExecutor();
        }
        m_future = m_executor.schedule(this, RETRY_DELAY, TimeUnit.SECONDS);
    }
}