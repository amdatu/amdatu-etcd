/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.util;

import java.net.URI;
import java.util.Dictionary;

import org.osgi.service.cm.ConfigurationException;

/**
 * Configuration map utilities.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ConfigurationUtil {

    public static String getStringProperty(Dictionary<String, ?> properties, String property, String defaultValue,
        boolean required) throws ConfigurationException {
        Object value = properties.get(property);
        if (value == null) {
            value = defaultValue;
        }
        if (value == null && required) {
            throw new ConfigurationException(property, "missing required property");
        }
        return value == null ? null : value.toString().trim();
    }

    public static String getStringProperty(Dictionary<String, ?> properties, String property)
        throws ConfigurationException {
        return getStringProperty(properties, property, null, false);
    }

    public static String getStringProperty(Dictionary<String, ?> properties, String property, String defaultValue)
        throws ConfigurationException {
        return getStringProperty(properties, property, defaultValue, false);
    }

    public static String getStringProperty(Dictionary<String, ?> properties, String property, boolean required)
        throws ConfigurationException {
        return getStringProperty(properties, property, null, required);
    }

    public static String[] getStringPlusProperty(Dictionary<String, ?> properties, String property,
        String defaultValue, boolean required) throws ConfigurationException {
        String value = getStringProperty(properties, property, defaultValue, required);
        return toStringArray(value);
    }

    public static String[] getStringPlusProperty(Dictionary<String, ?> properties, String property)
        throws ConfigurationException {
        return getStringPlusProperty(properties, property, null, false);
    }

    public static String[] getStringPlusProperty(Dictionary<String, ?> properties, String property, String defaultValue)
        throws ConfigurationException {
        return getStringPlusProperty(properties, property, defaultValue, false);
    }

    public static String[] getStringPlusProperty(Dictionary<String, ?> properties, String property, boolean required)
        throws ConfigurationException {
        return getStringPlusProperty(properties, property, null, required);
    }

    public static Integer getIntegerProperty(Dictionary<String, ?> properties, String property, String defaultValue,
        boolean required) throws ConfigurationException {
        String value = getStringProperty(properties, property, defaultValue, required);
        if (value == null) {
            return null;
        }
        try {
            return Integer.parseInt(value);
        }
        catch (Exception e) {
            throw new ConfigurationException(property, String.format("invalid int: %s", value), e);
        }
    }

    public static Integer getIntegerProperty(Dictionary<String, ?> properties, String property)
        throws ConfigurationException {
        return getIntegerProperty(properties, property, null, false);
    }

    public static Integer getIntegerProperty(Dictionary<String, ?> properties, String property, String defaultValue)
        throws ConfigurationException {
        return getIntegerProperty(properties, property, defaultValue, false);
    }

    public static Integer getIntegerProperty(Dictionary<String, ?> properties, String property, boolean required)
        throws ConfigurationException {
        return getIntegerProperty(properties, property, null, required);
    }

    public static Integer[] getIntegerPlusProperty(Dictionary<String, ?> properties, String property,
        String defaultValue, boolean required) throws ConfigurationException {
        String value = getStringProperty(properties, property, defaultValue, required);
        String[] parts = toStringArray(value);
        Integer[] values = new Integer[parts.length];
        try {
            for (int i = 0; i < parts.length; i++) {
                values[i] = Integer.parseInt(parts[i]);
            }
            return values;
        }
        catch (Exception e) {
            throw new ConfigurationException(property, String.format("invalid int array: %s", value), e);
        }
    }

    public static Integer[] getIntegerPlusProperty(Dictionary<String, ?> properties, String property)
        throws ConfigurationException {
        return getIntegerPlusProperty(properties, property, null, false);
    }

    public static Integer[] getIntegerPlusProperty(Dictionary<String, ?> properties, String property,
        String defaultValue) throws ConfigurationException {
        return getIntegerPlusProperty(properties, property, defaultValue, false);
    }

    public static Integer[] getIntegerPlusProperty(Dictionary<String, ?> properties, String property, boolean required)
        throws ConfigurationException {
        return getIntegerPlusProperty(properties, property, null, required);
    }

    public static URI getURIProperty(Dictionary<String, ?> properties, String property, String defaultValue,
        boolean required) throws ConfigurationException {
        String value = getStringProperty(properties, property, defaultValue, required);
        if (value == null) {
            return null;
        }
        try {
            return URI.create(value);
        }
        catch (Exception e) {
            throw new ConfigurationException(property, String.format("invalid uri: %s", value), e);
        }
    }

    public static URI getURIProperty(Dictionary<String, ?> properties, String property) throws ConfigurationException {
        return getURIProperty(properties, property, null, false);
    }

    public static URI getURIProperty(Dictionary<String, ?> properties, String property, String defaultValue)
        throws ConfigurationException {
        return getURIProperty(properties, property, defaultValue, false);
    }

    public static URI getURIProperty(Dictionary<String, ?> properties, String property, boolean required)
        throws ConfigurationException {
        return getURIProperty(properties, property, null, required);
    }

    public static URI[] getUriPlusProperty(Dictionary<String, ?> properties, String property, String defaultValue,
        boolean required) throws ConfigurationException {
        String value = getStringProperty(properties, property, defaultValue, required);
        String[] parts = toStringArray(value);
        URI[] values = new URI[parts.length];
        try {
            for (int i = 0; i < parts.length; i++) {
                values[i] = URI.create(parts[i]);
            }
            return values;
        }
        catch (Exception e) {
            throw new ConfigurationException(property, String.format("invalid uri array: %s", value), e);
        }
    }

    public static URI[] getUriPlusProperty(Dictionary<String, ?> properties, String property)
        throws ConfigurationException {
        return getUriPlusProperty(properties, property, null, false);
    }

    public static URI[] getUriPlusProperty(Dictionary<String, ?> properties, String property, String defaultValue)
        throws ConfigurationException {
        return getUriPlusProperty(properties, property, defaultValue, false);
    }

    public static URI[] getUriPlusProperty(Dictionary<String, ?> properties, String property, boolean required)
        throws ConfigurationException {
        return getUriPlusProperty(properties, property, null, required);
    }

    private ConfigurationUtil() {

    }

    private static String[] toStringArray(String value) {
        String[] parts = value.trim().split(",");
        for (int i = 0; i < parts.length; i++) {
            parts[i] = parts[i].trim();
        }
        return parts;
    }
}
