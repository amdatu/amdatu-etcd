/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.util;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osgi.framework.ServiceReference;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import mousio.etcd4j.promises.EtcdResponsePromise;
import mousio.etcd4j.responses.EtcdKeyAction;
import mousio.etcd4j.responses.EtcdKeysResponse;
import mousio.etcd4j.responses.EtcdKeysResponse.EtcdNode;

/**
 * Miscellaneous static internal utilities.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class EtcdUtil {

    public static final String SAFE_KEY_REGEX = "^[0-9a-zA-Z][0-9a-zA-Z-_.]*";
    private static final Pattern SAFE_KEY_PATTERN = Pattern.compile(SAFE_KEY_REGEX);

    private static final ObjectMapper OBJECTMAPPER = new ObjectMapper(new JsonFactory());
    private static final TypeReference<Hashtable<String, Object>> TYPEREFERENCE =
        new TypeReference<Hashtable<String, Object>>() {
        };

    public static Dictionary<String, Object> toDictionary(String json) {
        try {
            return OBJECTMAPPER.readValue(json, TYPEREFERENCE);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return new Hashtable<>();
    }

    public static String toJson(Dictionary<String, Object> config) {
        try {
            return OBJECTMAPPER.writeValueAsString(toMap(config));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static Map<String, Object> toMap(Dictionary<String, Object> dictionary) {
        Map<String, Object> map = new HashMap<>();
        Enumeration<String> keys = dictionary.keys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            map.put(key, dictionary.get(key));
        }
        return map;
    }

    public static boolean isUpdateAction(EtcdKeyAction action) {
        return action == EtcdKeyAction.compareAndSwap
            || action == EtcdKeyAction.create
            || action == EtcdKeyAction.update
            || action == EtcdKeyAction.set;
    }

    public static boolean isRemoveAction(EtcdKeyAction action) {
        return action == EtcdKeyAction.compareAndDelete
            || action == EtcdKeyAction.delete
            || action == EtcdKeyAction.expire;
    }

    public static long getModified(EtcdKeysResponse response) {
        long index = 0l;
        if (response.etcdIndex != null) {
            index = response.etcdIndex;
        }
        if (response.node.modifiedIndex > index) {
            index = response.node.modifiedIndex;
        }
        if (response.node.dir && response.node.nodes != null) {
            for (EtcdNode node : response.node.nodes) {
                if (node.modifiedIndex > index) {
                    index = node.modifiedIndex;
                }
            }
        }
        return index;
    }

    public static String getStringProperty(ServiceReference<?> reference,
        String key) {
        Object value = reference.getProperty(key);
        if (value == null || (!(value instanceof String))) {
            return null;
        }
        return (String) value;
    }

    public static boolean isSafeTopic(String value) {
        Matcher m = SAFE_KEY_PATTERN.matcher(value);
        return m.matches() && !value.matches("topics");
    }

    public static boolean isEmpty(String value) {
        return value == null || value.equals("");
    }

    public static void cancel(EtcdResponsePromise<?>... promises) {
        if (promises != null) {
            for (EtcdResponsePromise<?> promise : promises) {
                if (promise != null) {
                    try {
                        promise.cancel();
                    }
                    catch (Exception e) {
                        // silent
                    }
                }
            }
        }
    }

    public static void cancel(Future<?>... futures) {
        if (futures != null) {
            for (Future<?> future : futures) {
                if (future != null) {
                    try {
                        future.cancel(true);
                    }
                    catch (Exception e) {
                        // silent
                    }
                }
            }
        }
    }

    private EtcdUtil() {
    }
}
