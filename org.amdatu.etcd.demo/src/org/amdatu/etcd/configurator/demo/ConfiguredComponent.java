/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.configurator.demo;

import java.util.Dictionary;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ConfigurationDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.cm.ConfigurationException;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Component
public class ConfiguredComponent {

    @ConfigurationDependency(pid = "org.amdatu.etcd.configurator.demo.configured")
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        if (properties == null) {
            System.out.println("DEMO Configured: Configuration deleted");
            return;
        }
        System.out.println("DEMO Configured: Configuration updated");
        System.out.println("DEMO Configured: -> data " + properties);
    }

    @Start
    public void start() throws Exception {
        System.out.println("DEMO Configured: Starting...");
    }

    @Stop
    public void stop() throws Exception {
        System.out.println("DEMO Configured: stopping");
    }
}