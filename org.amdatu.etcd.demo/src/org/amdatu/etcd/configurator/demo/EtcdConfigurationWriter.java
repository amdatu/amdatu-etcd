/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.configurator.demo;

import static org.amdatu.etcd.util.ConfigurationUtil.getStringProperty;
import static org.amdatu.etcd.util.ConfigurationUtil.getUriPlusProperty;
import static org.amdatu.etcd.util.EtcdUtil.toJson;

import java.net.URI;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ConfigurationDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.service.cm.ConfigurationException;

import mousio.etcd4j.EtcdClient;

/**
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Component
public class EtcdConfigurationWriter {

    private volatile URI[] m_etcdEndpoints;
    private volatile String m_basePath;

    @SuppressWarnings("restriction")
    @ConfigurationDependency(pid = "org.amdatu.etcd.configurator.demo.configuring")
    public void updated(Dictionary<String, ?> properties) throws ConfigurationException {
        if (properties == null) {
            return;
        }
        m_etcdEndpoints = getUriPlusProperty(properties, "etcdEndpoints", "http://127.0.0.1:2379/v2/");
        m_basePath = getStringProperty(properties, "basePath", "/org.amdatu.etcd/configurator");
    }

    @Start
    public void start() throws Exception {
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(new Runnable() {

            @Override
            public void run() {
                runUpdates();
                executor.shutdown();
            }
        }, 1, TimeUnit.SECONDS);
    }

    @SuppressWarnings("restriction")
    private void runUpdates() {
        try {
            System.out.println("DEMO Configurator: Starting...");

            EtcdClient client = new EtcdClient(m_etcdEndpoints);
            String pid = "org.amdatu.etcd.configurator.demo.configured";

            Dictionary<String, Object> properties = new Hashtable<>();
            properties.put("prop1", "val1");
            properties.put("prop2", "val2");
            String path = m_basePath + "/" + pid;
            String data = toJson(properties);

            System.out.println("DEMO Configurator: Creating configuration in ETCD");
            System.out.println("DEMO Configurator: -> path " + path);
            System.out.println("DEMO Configurator: -> data " + data);
            client.put(path, data).send().get();

            TimeUnit.SECONDS.sleep(5);
            properties.put("prop1", "val1!");
            data = toJson(properties);
            System.out.println("DEMO Configurator: Updating configuration in ETCD");
            System.out.println("DEMO Configurator: -> path " + path);
            System.out.println("DEMO Configurator: -> data " + data);
            client.put(path, data).send().get();

            TimeUnit.SECONDS.sleep(5);
            System.out.println("DEMO Configurator: Deleting configuration in ETCD");
            System.out.println("DEMO Configurator: -> path " + path);
            client.delete(path).send().get();
            client.close();

            Thread.sleep(1);
            System.out.println("DEMO Configurator: Done...");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}