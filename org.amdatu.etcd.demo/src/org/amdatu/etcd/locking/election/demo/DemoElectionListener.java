/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import org.amdatu.etcd.locking.ElectionEvent;
import org.amdatu.etcd.locking.ElectionListener;
import org.amdatu.etcd.locking.ElectionManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Init;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@Component
public class DemoElectionListener implements ElectionListener {

    @Inject
    private volatile org.apache.felix.dm.Component m_component;

    @ServiceDependency(required = true)
    private volatile ElectionManager m_manager;

    @Init
    public void init() {
        Dictionary<String, Object> props = m_component.getServiceProperties();
        if (props == null) {
            props = new Hashtable<>();
        }
        props.put("osgi.command.scope", "election");
        props.put("osgi.command.function", new String[] { "status" });
        m_component.setServiceProperties(props);
    }

    @Start
    public void start() throws Exception {
        printMsg("******************************");
        printMsg("note: status command available");
        printMsg("******************************");
    }

    @Override
    public void handle(ElectionEvent event) {

        switch (event.getType()) {
            case ELECTION_ADDED:
                printMsg("election added - topic=%s", event.getTopic());
                break;
            case ELECTION_REMOVED:
                printMsg("election removed -topic=%s", event.getTopic());
                break;
            case CANDIDATE_ADDED:
                printMsg("candidate added - topic=%s identity=%s", event.getTopic(), event.getIdentity());
                break;
            case CANDIDATE_REMOVED:
                printMsg("candidate removed - topic=%s identity=%s", event.getTopic(), event.getIdentity());
                break;
            case LEADER_CHANGED:
                printMsg("leader changed - topic=%s identity=%s", event.getTopic(), event.getIdentity());
                break;
            default:
                printMsg("UNKNOWN EVENT TYPE: " + event.getType());
                break;
        }
    }

    private void printMsg(String msg, Object... args) {
        System.out.println("listener: " + String.format(msg, args));
    }

    public void status() {
        List<String> topics = new ArrayList<String>(m_manager.getTopics());
        Collections.sort(topics);
        for (String topic : topics) {
            System.out.println("election   : " + topic);
            System.out.println(" leader    : " + m_manager.getLeader(topic));
            List<String> candidates = new ArrayList<String>(m_manager.getCandidates(topic));
            Collections.sort(candidates);
            System.out.println(" candidates: " + candidates);
        }
    }
}
