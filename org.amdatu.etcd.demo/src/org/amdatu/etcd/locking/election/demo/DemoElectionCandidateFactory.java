/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election.demo;

import java.util.Dictionary;
import java.util.Hashtable;

import org.amdatu.etcd.locking.ElectionCandidate;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.annotation.api.FactoryConfigurationAdapterService;
import org.apache.felix.dm.annotation.api.Init;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.cm.ConfigurationException;

/**
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
@FactoryConfigurationAdapterService(factoryPid = "demo")
public class DemoElectionCandidateFactory implements ElectionCandidate {

    @Inject
    private volatile Component m_component;

    private volatile boolean m_init = false;
    private volatile String m_topic;
    private volatile String m_identity;

    protected void updated(Dictionary<String, ?> config) throws ConfigurationException {
        String topic = (String) config.get("topic");
        if (topic == null || topic.equals("")) {
            throw new ConfigurationException("topic", "missing");
        }
        String identity = (String) config.get("identity");
        if (identity == null || identity.equals("")) {
            throw new ConfigurationException("identity", "missing");
        }
        m_topic = topic;
        m_identity = identity;
        if (m_init) {
            updateRegistration();
        }
        printMsg("updated!");
    }

    @Init
    public void init() {
        m_init = true;
        updateRegistration();
        printMsg("initialized!");
    }

    @Start
    public void start() {
        printMsg("started!");
    }

    @Stop
    public void stop() {
        printMsg("stopped!");
    }

    @Override
    public void elected() {
        printMsg("elected!");
    }

    @Override
    public void demoted() {
        printMsg("demoted!");
    }

    private void updateRegistration() {
        Dictionary<String, Object> props = m_component.getServiceProperties();
        if (props == null) {
            props = new Hashtable<>();
        }
        props.put(ElectionCandidate.ELECTION_TOPIC, m_topic);
        props.put(ElectionCandidate.ELECTION_IDENTITY, m_identity);
        m_component.setServiceProperties(props);
    }

    private void printMsg(String msg, Object... args) {
        System.out.println(String.format("candidate (%s/%s): ", m_topic, m_identity) + msg);
    }

}
