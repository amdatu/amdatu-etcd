/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.configurator.itest;

import static org.amdatu.etcd.configurator.impl.ConfiguratorComponent.CONFIGURATOR_CONFIG_BASEPATH;
import static org.amdatu.etcd.configurator.impl.ConfiguratorComponent.CONFIGURATOR_CONFIG_ETCDENDPOINTS;
import static org.amdatu.etcd.configurator.impl.ConfiguratorComponent.CONFIGURATOR_CONFIG_PID;
import static org.amdatu.etcd.util.EtcdUtil.toJson;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

import java.net.URI;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

import mousio.etcd4j.EtcdClient;

/**
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ConfiguratorTest {

    private final static String ETCD_ENPOINTS = "http://localhost:1234/v2/,http://localhost:2379/v2/";
    private final static String ETCD_PATH = "/org.amdatu.etcd/configurator-itest";

    private BundleContext m_context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
    private EtcdClient m_etcdClient;

    @SuppressWarnings("restriction")
    @Before
    public void setUp() throws Exception {
        configure(this).add(
            createConfiguration(CONFIGURATOR_CONFIG_PID).setSynchronousDelivery(true)
                .set(CONFIGURATOR_CONFIG_ETCDENDPOINTS, ETCD_ENPOINTS)
                .set(CONFIGURATOR_CONFIG_BASEPATH, ETCD_PATH).set("debugLogging", "true")
                .set("consoleLogging", "true"))
            .apply();

        String[] uriStrings = ETCD_ENPOINTS.split(",");
        URI[] uris = new URI[uriStrings.length];
        for (int i = 0; i < uriStrings.length; i++) {
            uris[i] = URI.create(uriStrings[i].trim());
        }
        m_etcdClient = new EtcdClient(uris);
    }

    @After
    public void tearDown() throws Exception {
        cleanUp(this);
        m_etcdClient.deleteDir(ETCD_PATH).recursive().send().get();
        m_etcdClient.close();
        m_etcdClient = null;
    }

    @Test
    public void testManagedServiceUpdates() throws Exception {

        String servicePID = "test.testManagedServiceUpdates";
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, servicePID);

        ManagedService service = mock(ManagedService.class);
        ServiceRegistration<?> registration = m_context.registerService(ManagedService.class, service, props);
        try {
            verify(service, timeout(3000)).updated(matchDictionary(null));
            reset(service);

            Dictionary<String, Object> configuration = toDictionary("key1", "value1");
            updateConfig(servicePID, configuration);
            verify(service, timeout(3000)).updated(matchDictionary(configuration));
            reset(service);

            configuration = toDictionary("key1", "value1", "key2", 2);
            updateConfig(servicePID, configuration);
            verify(service, timeout(3000)).updated(matchDictionary(configuration));
            reset(service);

            configuration = toDictionary("key1", "value1");
            updateConfig(servicePID, configuration);
            verify(service, timeout(3000)).updated(matchDictionary(configuration));
            reset(service);

            configuration = null;
            updateConfig(servicePID, configuration);
            verify(service, timeout(3000)).updated(matchDictionary(configuration));
            reset(service);

        }
        finally {
            registration.unregister();
        }
    }

    @Test
    public void testManagedServiceFactoryUpdates() throws Exception {

        String servicePID = "test.testManagedServiceFactoryUpdates";
        Dictionary<String, Object> props = new Hashtable<>();
        props.put(Constants.SERVICE_PID, servicePID);
        ManagedServiceFactory service = mock(ManagedServiceFactory.class);
        ServiceRegistration<?> registration = m_context.registerService(ManagedServiceFactory.class, service, props);

        ArgumentCaptor<String> pid1 = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> pid2 = ArgumentCaptor.forClass(String.class);
        try {

            Dictionary<String, Object> configuration = toDictionary("key1", "value1");
            updateConfig(servicePID + "-1", configuration);
            verify(service, timeout(5000)).updated(pid1.capture(), matchDictionary(configuration));
            reset(service);

            configuration = toDictionary("key1", "value1", "key2", 2);
            updateConfig(servicePID + "-1", configuration);
            verify(service, timeout(1000)).updated(eq(pid1.getValue()), matchDictionary(configuration));
            reset(service);

            updateConfig(servicePID + "-2", configuration);
            verify(service, timeout(1000)).updated(pid2.capture(), matchDictionary(configuration));
            reset(service);

            configuration = null;
            updateConfig(servicePID + "-1", configuration);
            verify(service, timeout(1000)).deleted(eq(pid1.getValue()));

            updateConfig(servicePID + "-2", configuration);
            verify(service, timeout(1000)).deleted(eq(pid2.getValue()));
        }
        finally {
            registration.unregister();
        }
    }

    private Dictionary<String, Object> toDictionary(Object... properties) {
        Dictionary<String, Object> dictionary = new Hashtable<>();
        if (properties != null) {
            for (int i = 0; i < properties.length - 1; i += 2) {
                dictionary.put(properties[i].toString(), properties[i + 1]);
            }
        }
        return dictionary;
    }

    @SuppressWarnings("restriction")
    private void updateConfig(String pid, Dictionary<String, Object> properties) throws Exception {
        if (properties == null) {
            m_etcdClient.delete(ETCD_PATH + "/" + pid).send().get();
        }
        else {
            m_etcdClient.put(ETCD_PATH + "/" + pid, toJson(properties)).send().get();
        }
    }

    private static Dictionary<String, Object> matchDictionary(Dictionary<String, Object> expected) {
        return argThat(new DictionaryMatcher(expected));
    }

    private static class DictionaryMatcher implements ArgumentMatcher<Dictionary<String, Object>> {

        private final Dictionary<String, Object> m_expected;

        public DictionaryMatcher(Dictionary<String, Object> expected) {
            m_expected = expected;
        }

        @Override
        public boolean matches(Dictionary<String, Object> dictionary) {
            if (dictionary == null) {
                return m_expected == null;
            }
            if (m_expected == null) {
                return false;
            }
            try {
                Enumeration<String> keys = m_expected.keys();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    if (dictionary.get(key) == null || !dictionary.get(key).equals(m_expected.get(key))) {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }
    }
}
