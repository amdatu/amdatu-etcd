/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election.itest;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogService;

/**
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class ConsolerLogger implements LogService, LogListener {

    @Override
    public void log(int level, String message) {
        System.out.println(getTimestamp() + " - " + getLevelName(level) + " - " + message);
    }

    @Override
    public void log(int level, String message, Throwable exception) {
        System.out.println(getTimestamp() + " - " + getLevelName(level) + " - " + message);
    }

    @Override
    public void log(@SuppressWarnings("rawtypes") ServiceReference sr, int level, String message) {
        System.out.println(getTimestamp() + " - " + getLevelName(level) + " - " + message);
    }

    @Override
    public void log(@SuppressWarnings("rawtypes") ServiceReference sr, int level, String message, Throwable exception) {
        System.out.println(getTimestamp() + " - " + getLevelName(level) + " - " + message);
    }

    @Override
    public void logged(LogEntry entry) {
        System.out.println(getTimestamp() + " - " + getLevelName(entry.getLevel()) + " - " + entry.getMessage());
    }

    private static final String getTimestamp() {
        return new SimpleDateFormat("HH:mm:ss:SSS").format(new Date());
    }

    private static final String getLevelName(int level) {
        switch (level) {
            case 1:
                return "ERROR";
            case 2:
                return "WARN ";
            case 3:
                return "INFO ";
            case 4:
                return "DEBUG";
            default:
                return "?????";
        }
    }
}
