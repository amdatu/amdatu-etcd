/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.etcd.locking.election.itest;

import static org.amdatu.etcd.locking.ElectionEvent.TYPE.CANDIDATE_ADDED;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.CANDIDATE_REMOVED;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.ELECTION_ADDED;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.ELECTION_REMOVED;
import static org.amdatu.etcd.locking.ElectionEvent.TYPE.LEADER_CHANGED;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_CANDIDATETTL;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_DEBUG;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_ETCDENDPOINTS;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_LEADERTTL;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_PID;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_RETRYDELAY;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_TOPICTTL;
import static org.amdatu.etcd.locking.election.ElectionComponent.ELECTION_CONFIG_UPDATEMARGIN;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.amdatu.etcd.locking.ElectionCandidate;
import org.amdatu.etcd.locking.ElectionEvent;
import org.amdatu.etcd.locking.ElectionListener;
import org.amdatu.etcd.locking.ElectionManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;

/**
 *
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class LeaderElectionTest {

    private final static String ETCD_ENPOINTS = "http://localhost:1234/v2/,http://localhost:2379/v2/";
    private static final int TEST_TOPICTTL = 5;

    private final BundleContext m_context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();

    private volatile ElectionManager m_manager;
    private volatile LogService m_logger;
    private volatile LogReaderService m_reader;

    private LogListener m_consolelogger = new ConsolerLogger();

    @SuppressWarnings("restriction")
    @Before
    public void setUp() {
        configure(this)
            .add(createConfiguration(ELECTION_CONFIG_PID).setSynchronousDelivery(true)
                .set(ELECTION_CONFIG_ETCDENDPOINTS, ETCD_ENPOINTS).set(ELECTION_CONFIG_TOPICTTL, "" + TEST_TOPICTTL)
                .set(ELECTION_CONFIG_LEADERTTL, "2").set(ELECTION_CONFIG_CANDIDATETTL, "5")
                .set(ELECTION_CONFIG_UPDATEMARGIN, "1").set(ELECTION_CONFIG_RETRYDELAY, "1")
                .set(ELECTION_CONFIG_DEBUG, "true"))
            .add(createServiceDependency().setService(ElectionManager.class))
            .add(createServiceDependency().setService(LogService.class))
            .add(createServiceDependency().setService(LogReaderService.class)).apply();

        m_reader.addLogListener(m_consolelogger);
    }

    @After
    public void tearDown() {
        m_reader.removeLogListener(m_consolelogger);
        cleanUp(this);
    }

    @Test
    public void testInvalidTopicIgnored() throws Exception {
        log("************************ testInvalidTopicIgnored ***************************");
        // TODO listeners can have empty topic
        // doTestInvalidTopicIgnored("");
        doTestInvalidTopicIgnored(" ");
        doTestInvalidTopicIgnored("foo ");
        doTestInvalidTopicIgnored(" foo");
        doTestInvalidTopicIgnored("foo/bar");
        doTestInvalidTopicIgnored("foo\\bar");
        doTestInvalidTopicIgnored("_");
        log("*********************************** DONE ************************************");
    }

    @Test
    public void testValidTopicAccepted() throws Exception {
        log("************************ testValidTopicAccepted ***************************");
        doTestValidTopicAccepted("foo");
        doTestValidTopicAccepted("foo.bar1");
        doTestValidTopicAccepted("foo2-bar");
        doTestValidTopicAccepted("foo_bar_4");
        log("*********************************** DONE ************************************");
    }

    public void doTestValidTopicAccepted(String topic) throws Exception {
        log("**** START doTestValidTopicAccepted: " + topic);
        ElectionCandidate candidate = mock(ElectionCandidate.class);
        ElectionListener listener = mock(ElectionListener.class);
        ServiceRegistration<?> listenerReg = null;
        ServiceRegistration<?> candidateReg = null;
        try {
            listenerReg = m_context.registerService(ElectionListener.class, listener, getListenerProperties(topic));
            candidateReg = m_context.registerService(ElectionCandidate.class, candidate,
                getCandidateProperties("candidate", topic));

            TimeUnit.SECONDS.sleep(1);
            verify(listener).handle(matchEvent(ELECTION_ADDED, topic, null));
            verify(listener).handle(matchEvent(CANDIDATE_ADDED, topic, "candidate"));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, "candidate"));
            reset(listener);

            assertTrue(m_manager.getTopics().contains(topic));

            candidateReg = closeRegistration(candidateReg);

            TimeUnit.SECONDS.sleep(TEST_TOPICTTL + 1);
            verify(listener).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate"));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, null));
            verify(listener).handle(matchEvent(ELECTION_REMOVED, topic, null));
            reset(listener);

            assertFalse(m_manager.getTopics().contains(topic));

        }
        finally {
            candidateReg = closeRegistration(candidateReg);
            listenerReg = closeRegistration(listenerReg);
        }
        log("**** DONE doTestValidTopicAccepted: " + topic);
    }

    public void doTestInvalidTopicIgnored(String topic) throws Exception {
        log("**** START doTestInvalidTopicIgnored: " + topic);
        ElectionCandidate candidate = mock(ElectionCandidate.class);
        ElectionListener listener = mock(ElectionListener.class);
        ServiceRegistration<?> listenerReg = null;
        ServiceRegistration<?> candidateReg = null;
        try {
            listenerReg = m_context.registerService(ElectionListener.class, listener, getListenerProperties(topic));
            candidateReg = m_context.registerService(ElectionCandidate.class, candidate,
                getCandidateProperties("candidate", topic));

            TimeUnit.SECONDS.sleep(1);
            verify(listener, never()).handle(any(ElectionEvent.class));
            assertFalse(m_manager.getTopics().contains(topic));
        }
        catch (Exception e) {
            candidateReg = closeRegistration(candidateReg);
            listenerReg = closeRegistration(listenerReg);
        }
        log("**** DONE doTestValidTopicAccepted: " + topic);
    }

    private static ServiceRegistration<?> closeRegistration(ServiceRegistration<?> registration) {
        if (registration != null) {
            registration.unregister();
        }
        return null;
    }

    @Test
    public void testCandidateListenerCallbacks() throws Exception {
        log("************************ testCandidateListenerCallbacks ***************************");
        String topic = "testCandidateListenerCallbacks";
        String identity1 = "dabbert";
        String identity2 = "candidate2";

        ElectionCandidate candidate1 = mock(ElectionCandidate.class);
        Dictionary<String, Object> candidate1Props = getCandidateProperties(identity1, topic);

        ElectionCandidate candidate2 = mock(ElectionCandidate.class);
        Dictionary<String, Object> candidate2Props = getCandidateProperties(identity2, topic);

        ElectionListener listener = mock(ElectionListener.class);
        Dictionary<String, Object> listenerProps = getListenerProperties(topic);

        ElectionListener listener2 = new ElectionListener() {

            @Override
            public void handle(ElectionEvent event) {
                System.out.println(">>> " + event);
            }
        };
        Dictionary<String, Object> listenerProps2 = getListenerProperties(topic);

        ServiceRegistration<?> registration1 = null;
        ServiceRegistration<?> registration2 = null;
        ServiceRegistration<?> registration3 = null;
        ServiceRegistration<?> registration4 = null;

        try {

            log("**** Register Candidate 1");
            registration1 = m_context.registerService(ElectionCandidate.class, candidate1, candidate1Props);

            log("**** Register Listener 1");
            registration3 = m_context.registerService(ElectionListener.class, listener, listenerProps);

            TimeUnit.SECONDS.sleep(1);
            verify(candidate1).elected();
            verify(listener).handle(matchEvent(ELECTION_ADDED, topic, null));
            verify(listener).handle(matchEvent(CANDIDATE_ADDED, topic, identity1));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, identity1));
            reset(candidate1, candidate2, listener);

            log("**** Register Candidate 2");
            registration2 = m_context.registerService(ElectionCandidate.class, candidate2, candidate2Props);
            TimeUnit.SECONDS.sleep(1);
            verify(listener).handle(matchEvent(CANDIDATE_ADDED, topic, identity2));
            reset(candidate1, candidate2, listener);

            log("**** Deregister Listener 1");
            registration3.unregister();

            log("**** Register Listener 1");
            registration3 = m_context.registerService(ElectionListener.class, listener, listenerProps);
            TimeUnit.SECONDS.sleep(1);
            verify(listener).handle(matchEvent(ELECTION_ADDED, topic, null));
            verify(listener).handle(matchEvent(CANDIDATE_ADDED, topic, identity1));
            verify(listener).handle(matchEvent(CANDIDATE_ADDED, topic, identity2));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, identity1));
            reset(candidate1, candidate2, listener);

            log("**** Deregister Candidate 1");
            registration1.unregister();
            TimeUnit.SECONDS.sleep(1);
            verify(candidate2).elected();
            verify(listener).handle(matchEvent(CANDIDATE_REMOVED, topic, identity1));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, identity2));
            reset(candidate1, candidate2, listener);

            log("**** Register Candidate 1");
            registration1 = m_context.registerService(ElectionCandidate.class, candidate1, candidate1Props);
            TimeUnit.SECONDS.sleep(1);
            verify(listener).handle(matchEvent(CANDIDATE_ADDED, topic, identity1));

            log("**** Deregister Candidate 2");
            registration2.unregister();
            TimeUnit.SECONDS.sleep(1);
            verify(candidate1).elected();
            verify(listener).handle(matchEvent(CANDIDATE_REMOVED, topic, identity2));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, identity1));
            reset(candidate1, candidate2, listener);

            log("**** Register Listener 2");
            registration4 = m_context.registerService(ElectionListener.class, listener2, listenerProps2);
            Thread.sleep(100);
            log("**** Deregister Candidate 1");
            registration1.unregister();
            TimeUnit.SECONDS.sleep(TEST_TOPICTTL + 1);
            verify(listener).handle(matchEvent(CANDIDATE_REMOVED, topic, identity1));
            verify(listener).handle(matchEvent(LEADER_CHANGED, topic, null));
            verify(listener).handle(matchEvent(ELECTION_REMOVED, topic, null));
        }
        catch (Exception e) {
            unregister(registration1);
            unregister(registration2);
            unregister(registration3);
            unregister(registration4);
        }
        log("*********************************** DONE ************************************");
    }

    @Ignore
    @Test
    public void testWithManyCandidates() throws Exception {

        String topic = "manyCandidatesTest";
        int many = 10;

        log("************************ Register listener 1 ***************************");
        ElectionListener listener1 = mock(ElectionListener.class);
        ServiceRegistration<?> listener1Reg = m_context.registerService(ElectionListener.class, listener1,
            getListenerProperties(topic));

        log("************************ Register " + many + " candidates ***************************");
        List<ServiceRegistration<?>> registrations = new ArrayList<>();
        for (int i = 0; i < many; i++) {
            ServiceRegistration<?> registration = m_context.registerService(ElectionCandidate.class,
                mock(ElectionCandidate.class), getCandidateProperties("candidate" + i, topic));
            registrations.add(registration);
        }

        log("************************ Verify listener 1 ***************************");
        TimeUnit.SECONDS.sleep(1);
        verify(listener1).handle(matchEvent(ELECTION_ADDED, topic, null));
        verify(listener1).handle(matchEvent(LEADER_CHANGED, topic, "candidate0"));
        for (int i = 0; i < many; i++) {
            verify(listener1).handle(matchEvent(CANDIDATE_ADDED, topic, "candidate" + i));
        }
        reset(listener1);

        log("************************ Register listener 2 ***************************");
        ElectionListener listener2 = mock(ElectionListener.class);
        ServiceRegistration<?> listener2Reg = m_context.registerService(ElectionListener.class, listener2,
            getListenerProperties(topic));

        log("************************ Verify listener 2 ***************************");
        TimeUnit.SECONDS.sleep(1);
        verify(listener2).handle(matchEvent(ELECTION_ADDED, topic, null));
        verify(listener2).handle(matchEvent(LEADER_CHANGED, topic, "candidate0"));
        for (int i = 0; i < many; i++) {
            verify(listener2).handle(matchEvent(CANDIDATE_ADDED, topic, "candidate" + i));
        }
        reset(listener2);

        log("************************ Deregister candidate 0 ***************************");
        ServiceRegistration<?> registration1 = registrations.remove(0);
        registration1.unregister();

        log("************************ Verify listener 1 ***************************");
        TimeUnit.SECONDS.sleep(1);
        verify(listener1).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate0"));
        verify(listener1).handle(matchEvent(LEADER_CHANGED, topic, "candidate1"));
        reset(listener1);

        log("************************ Verify listener 2 ***************************");
        TimeUnit.SECONDS.sleep(1);
        verify(listener2).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate0"));
        verify(listener2).handle(matchEvent(LEADER_CHANGED, topic, "candidate1"));
        reset(listener2);

        log("************************ Deregister candidates 1 & 2 ***************************");
        ServiceRegistration<?> registration2 = registrations.remove(0);
        ServiceRegistration<?> registration3 = registrations.remove(0);
        registration3.unregister();
        registration2.unregister();

        log("************************ Verify listener 1 ***************************");
        TimeUnit.SECONDS.sleep(1);
        verify(listener1).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate1"));
        verify(listener1).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate2"));
        verify(listener1).handle(matchEvent(LEADER_CHANGED, topic, "candidate3"));
        reset(listener1);

        log("************************ Verify listener 2 ***************************");
        TimeUnit.SECONDS.sleep(1);
        verify(listener2).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate1"));
        verify(listener2).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate2"));
        verify(listener2).handle(matchEvent(LEADER_CHANGED, topic, "candidate3"));
        reset(listener2);

        log("************************ Deregister candidates ***************************");
        for (ServiceRegistration<?> registration : registrations) {
            registration.unregister();
        }

        log("************************ Verify listener 2 ***************************");
        TimeUnit.SECONDS.sleep(5);
        for (int i = 3; i < many; i++) {
            verify(listener2).handle(matchEvent(CANDIDATE_REMOVED, topic, "candidate" + i));
            if (i < many - 1)
                verify(listener2).handle(matchEvent(LEADER_CHANGED, topic, "candidate" + (i + 1)));
        }
        TimeUnit.SECONDS.sleep(TEST_TOPICTTL + 1);
        verify(listener2).handle(matchEvent(LEADER_CHANGED, topic, null));
        verify(listener2).handle(matchEvent(ELECTION_REMOVED, topic, null));
        reset(listener2);

        log("************************ Deregister listener 2 ***************************");
        listener1Reg.unregister();
        listener2Reg.unregister();
        log("*********************************** DONE ************************************");
    }

    private Dictionary<String, Object> getCandidateProperties(String identity, String topic) {
        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(ElectionCandidate.ELECTION_IDENTITY, identity);
        properties.put(ElectionCandidate.ELECTION_TOPIC, topic);
        return properties;
    }

    private Dictionary<String, Object> getListenerProperties(String topic) {
        Dictionary<String, Object> properties = new Hashtable<>();
        properties.put(ElectionListener.ELECTION_TOPIC, topic);
        return properties;
    }

    private void unregister(ServiceRegistration<?> registration) {
        if (registration != null) {
            registration.unregister();
        }
    }

    private void log(String s) {
        m_logger.log(LogService.LOG_INFO, s);
    }

    private static ElectionEvent matchEvent(ElectionEvent.TYPE type, String topic, String identity) {
        return argThat(new ElectionEventMatcher(type, topic, identity));
    }

    private static class ElectionEventMatcher implements ArgumentMatcher<ElectionEvent> {

        private ElectionEvent.TYPE m_type;
        private String m_topic;
        private String m_identity;

        public ElectionEventMatcher(ElectionEvent.TYPE type, String topic, String identity) {
            m_type = type;
            m_topic = topic;
            m_identity = identity;
        }

        public boolean matches(ElectionEvent oe) {
            if (m_type != oe.getType() || !m_topic.equals(oe.getTopic()))
                return false;
            if (m_identity == null && (oe.getIdentity() != null))
                return false;
            if (m_identity != null && !m_identity.equals(oe.getIdentity()))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "type: " + m_type + " topic: " + m_topic + " identity: " + m_identity;
        }
    }
}
