# Amdatu Etcd

Amdatu Etcd provides a collection of services and utilities on top of and Etcd backend.

[wiki] (https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Etcd)


## Continuous build and release

This projects uses bitbucket pipelines for continuous builds and automation of snapshot and release deployment to AWS S3.


### Snapshots

The _snapshot_ task creates a local snapshot repository against the configured _baseline_ repository, that should point to the latest formal release repository.

The _s3SnapshotDeploy_ task depends on the _snapshot_ and _asciidoctor_ tasks and subsequently deploys the generated snapshot repository to http://amdatu-repo.s3.amazonaws.com/amdatu-etcd/snapshot/repo/index.xml.gz and docs to http://amdatu-repo.s3.amazonaws.com/amdatu-etcd/snapshot/docs/index.html.

Pipelines is configured to automatically execute _s3SnapshotDeploy_ for every build of the _master_ branch.

### Releases

The standard bnd _release_ task creates a local release repository (see _cnf/build.bnd_).

The _s3ReleaseDeploy_ task depends on the _release_ and _asciidoctor_ tasks and subsequently deploys the generated release repository to http://amdatu-repo.s3.amazonaws.com/amdatu-etcd/<version>/repo/index.xml.gz  and docs to http://amdatu-repo.s3.amazonaws.com/amdatu-etcd/<version>/docs/index.html.

Pipelines is configured to execute _s3release_ for every build of a tag name _r*_.


### Configuration

BND repositories are configured in _cnf/build.bnd_.

Bitbucket Pipelines is configured in _bitbucket-pipelines.yml_.

Gradle task configuration is in _gradle.properties_.

AWS credentials must be provided by Setting the environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.

